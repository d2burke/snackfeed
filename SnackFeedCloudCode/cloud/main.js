var saveResponse, invites = [], errors = [], invitedUsers = [], emailsSent = [];

Parse.Cloud.define("sendItemStatusNotification", function(request, response) {
    saveResponse = response;
    sendItemStatusNotification(request.params);
});

Parse.Cloud.define("upsertLocation", function(request, response) {
    saveResponse = response;
    upsertLocation(request.params);
});

function sendItemStatusNotification(params){
    query = new Parse.Query("Nom");
    query.equalTo("objectId", params.ItemId);
    query.first({
        success: function(item){
        	item.fetch();

        	var office = item.get("Office");
        	office.fetch();

        	var user = item.get("User");
        	user.fetch({
        		success: function(user){
		            Parse.Push.send({
		              channels: [ "office-" + office.id + "" ],
		              data: {
		                 alert: user.get("username") + " updated " + item.get("Description") + " to " + item.get("Status")
		              }
		            }, { success: function() {
			                saveResponse.success("Success");
		              }, error: function(err) { 
			                saveResponse.error(err);
		              }
		            });
        		},
        		error: function(err){
        			saveResponse.error(err);
        		}
        	});
        },
        error: function(error){

        }
    });
}

function upsertLocation(params){
	var response = {};
    query = new Parse.Query("Office");
    query.equalTo("Name", params.name);
    query.first({
        success: function(item){
        	response.Location = item;
        	//If exists, send back Location info
        	if(item){
        		var office = item.get("Office");
        		if(office){
	        		office.fetch();
        		}

			    query = new Parse.Query("Nom");
			    query.equalTo("Office", office);
			    query.first({
			        success: function(snacks){
			        	response.Snacks = snacks || [];
			        	saveResponse.success(response);
			        },
			        error: function(err){
			        	saveResponse.error(err);
			        }
			    });
        	}
        	else{
	            var Office = Parse.Object.extend("Office");
	            var location = new Office();
                location.set("Name", params.name);
                location.set("Address", params.vicinity);
	            location.set("Location", new Parse.GeoPoint(params.geometry.location.lat, params.geometry.location.lng));
	            location.save({
	            	success: function(location){
	            		response = {
	            			"Location" : location
	            		}
			        	saveResponse.success(response);
	            	},
	            	error: function(err){
			        	saveResponse.error(err);
	            	}
	            });
        	}
        },
        error: function(err){
        	saveResponse.error(err);
        }
    });
}