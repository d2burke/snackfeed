//
//  CommentActivityCell.h
//  SnackFeed
//
//  Created by Daniel Burke on 2/6/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentActivityCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@end
