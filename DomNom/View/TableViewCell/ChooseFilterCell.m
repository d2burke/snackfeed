//
//  ChooseFilterCell.m
//  SnackFeed
//
//  Created by Daniel.Burke on 2/10/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import "ChooseFilterCell.h"

@implementation ChooseFilterCell

- (void)layoutSubviews{
    [super layoutSubviews];
    _selectionIcon.layer.cornerRadius = 10.f;
    _selectionIcon.layer.borderColor = DARK_GRAY.CGColor;
    _selectionIcon.layer.borderWidth = 1.f;
    _underline = [CALayer layer];
    _underline.frame = CGRectMake(0, 5, self.frame.size.width, 1);
    _underline.backgroundColor = LIGHT_GRAY.CGColor;
    [self.layer addSublayer:_underline];
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    _selectionIcon.backgroundColor = (selected) ? DARK_GRAY : LIGHT_GRAY;
    // Configure the view for the selected state
}

@end
