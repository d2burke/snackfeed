//
//  PhotoActivityCell.m
//  SnackFeed
//
//  Created by Daniel Burke on 2/6/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import "PhotoActivityCell.h"

@implementation PhotoActivityCell

- (void)drawRect:(CGRect)rect{
    _userImageView.layer.cornerRadius = 25.f;
    _photoImageView.layer.borderColor = LIGHT_GRAY.CGColor;
    _photoImageView.layer.borderWidth = 1.f;
}

@end
