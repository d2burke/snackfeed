//
//  StatusActivityCell.m
//  SnackFeed
//
//  Created by Daniel Burke on 2/6/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import "StatusActivityCell.h"

@implementation StatusActivityCell

- (void)drawRect:(CGRect)rect{
    _userImageView.layer.cornerRadius = 25.f;
}

@end
