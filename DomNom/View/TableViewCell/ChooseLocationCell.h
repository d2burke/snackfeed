//
//  ChooseLocationCell.h
//  SnackFeed
//
//  Created by Daniel.Burke on 1/28/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseLocationCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIView *selectionIcon;

@end
