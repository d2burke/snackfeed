//
//  ChooseFilterCell.h
//  SnackFeed
//
//  Created by Daniel.Burke on 2/10/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseFilterCell : UITableViewCell

@property (weak, nonatomic) CALayer *underline;
@property (weak, nonatomic) IBOutlet UILabel *filterLabel;
@property (weak, nonatomic) IBOutlet UILabel *filterIcon;
@property (weak, nonatomic) IBOutlet UIView *selectionIcon;
@end
