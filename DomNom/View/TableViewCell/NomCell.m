//
//  NomCell.m
//  SnackFeed
//
//  Created by Daniel.Burke on 1/23/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import "NomCell.h"

@implementation NomCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)drawRect:(CGRect)rect{
    self.contentView.backgroundColor = [UIColor lightGrayColor];
    _userImageView.layer.cornerRadius = 25.f;
    
    CGRect gradientFrame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    UIColor *colorOne = [[UIColor blackColor] colorWithAlphaComponent:0.7];
    UIColor *colorTwo = [UIColor blackColor];
    UIColor *colorThree = [UIColor blackColor];
    UIColor *colorFour = [[UIColor blackColor] colorWithAlphaComponent:0.7];
    NSArray *colors =  [NSArray arrayWithObjects:(id)colorOne.CGColor, colorTwo.CGColor,colorThree.CGColor,colorFour.CGColor, nil];
    NSNumber *stopOne = [NSNumber numberWithFloat:0.0];
    NSNumber *stopTwo = [NSNumber numberWithFloat:0.3];
    NSNumber *stopThree = [NSNumber numberWithFloat:0.6];
    NSNumber *stopFour = [NSNumber numberWithFloat:1];
    NSArray *locations = [NSArray arrayWithObjects:stopOne, stopTwo,stopThree,stopFour, nil];
    CAGradientLayer *maskLayer = [CAGradientLayer layer];
    maskLayer.frame = gradientFrame;
    maskLayer.locations = locations;
    maskLayer.colors = colors;
    _nomImageView.layer.mask = maskLayer;
    
    CGRect actionViewFrame = _actionView.frame;
    actionViewFrame.origin.x = self.frame.size.width;
    _actionView.frame = actionViewFrame;
    
    _thankYouButton.layer.cornerRadius = 25.f;
}

- (void)layoutSubviews{
    if(_snackIsGone){
        [self removePanGesture];
    }
    else{
        [self addPanGesture];
    }
}

- (void)panCell:(UIPanGestureRecognizer*)recognizer{
    
    if([[self tableView] isDragging] || [[self tableView] isDecelerating]) return;
    [_delegate setTableScrollEnabed:NO];
    
    CGRect actionViewFrame = _actionView.frame;
    _actionView.hidden = NO;
    
    CGFloat startX = 0;
    switch (recognizer.state) {
        case UIGestureRecognizerStateBegan:
            startX = [recognizer translationInView:self].x;
        case UIGestureRecognizerStateChanged:{
            if([recognizer translationInView:self].x < 0){
                CGRect cellFrame = _containerView.frame;
                cellFrame.origin.x = startX + [recognizer translationInView:self].x;
                _containerView.frame = cellFrame;
                
                CGRect imageFrame = _nomImageView.frame;
                imageFrame.origin.x = startX - [recognizer translationInView:self].x/4;
                _nomImageView.frame = imageFrame;
                
                _statusNewIcon.hidden = YES;
                _statusNewLabel.hidden = YES;
                _statusAlmostIcon.hidden = YES;
                _statusAlmostLabel.hidden = YES;
                _statusGoneIcon.hidden = YES;
                _statusGoneLabel.hidden = YES;
                
                if([recognizer translationInView:self].x < 0){
                    if(-[recognizer translationInView:self].x > self.frame.size.width/2){
                        self.contentView.backgroundColor = [UIColor redColor];
                        _statusGoneLabel.hidden = NO;
                        _statusGoneIcon.hidden = NO;
                    }
                    else if(-[recognizer translationInView:self].x > self.frame.size.width/4){
                        self.contentView.backgroundColor = [UIColor orangeColor];
                        _statusAlmostLabel.hidden = NO;
                        _statusAlmostIcon.hidden = NO;
                    }
                    else{
                        self.contentView.backgroundColor = [UIColor lightGrayColor];
                        _statusNewLabel.hidden = NO;
                        _statusNewIcon.hidden = NO;
                    }
                }

                if(startX + [recognizer translationInView:self].x < -100){
                    CGFloat offset = (startX + [recognizer translationInView:self].x/2) - 50;
                    actionViewFrame.origin.x = self.frame.size.width + offset;
                }
                else{
                    actionViewFrame.origin.x = self.frame.size.width + (startX + [recognizer translationInView:self].x);
                }
                _actionView.frame = actionViewFrame;
            }
            
            break;
        }
        case UIGestureRecognizerStateCancelled:{
            [_delegate setTableScrollEnabed:YES];
            CGRect cellFrame = _containerView.frame;
            cellFrame.origin.x = 0;
            CGRect imageFrame = _nomImageView.frame;
            imageFrame.origin.x = 0;
            CGRect actionViewFrame = _actionView.frame;
            actionViewFrame.origin.x = self.frame.size.width;
            
            [UIView animateWithDuration:0.34 animations:^{
                _containerView.frame = cellFrame;
                _nomImageView.frame = imageFrame;
                _actionView.frame = actionViewFrame;
            } completion:^(BOOL finished) {
                _actionView.hidden = YES;
                self.contentView.backgroundColor = LIGHT_GRAY;
            }];
            [_delegate cell:self atIndexPath:[[self tableView] indexPathForCell:self] didPanToPosition:startX+[recognizer translationInView:self].x];
            break;
        }
        case UIGestureRecognizerStateEnded:{
            [_delegate setTableScrollEnabed:YES];
            CGRect cellFrame = _containerView.frame;
            cellFrame.origin.x = 0;
            CGRect imageFrame = _nomImageView.frame;
            imageFrame.origin.x = 0;
            CGRect actionViewFrame = _actionView.frame;
            actionViewFrame.origin.x = self.frame.size.width;
            [UIView animateWithDuration:0.34 animations:^{
                _containerView.frame = cellFrame;
                _nomImageView.frame = imageFrame;
                _actionView.frame = actionViewFrame;
            } completion:^(BOOL finished) {
                _actionView.hidden = YES;
                self.contentView.backgroundColor = LIGHT_GRAY;
            }];
            
            [_delegate cell:self atIndexPath:[[self tableView] indexPathForCell:self] didPanToPosition:startX+[recognizer translationInView:self].x];
            
            break;
        }
        default:{
            CGRect cellFrame = _containerView.frame;
            cellFrame.origin.x = 0;
            CGRect imageFrame = _nomImageView.frame;
            imageFrame.origin.x = 0;
            [UIView animateWithDuration:0.34 animations:^{
                _containerView.frame = cellFrame;
                _nomImageView.frame = imageFrame;
            } completion:^(BOOL finished) {
                _actionView.hidden = YES;
                self.contentView.backgroundColor = LIGHT_GRAY;
            }];
            [_delegate setTableScrollEnabed:YES];
            break;
        }
    }
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    return YES;
}

- (BOOL)gestureRecognizerShouldBegin:(UIPanGestureRecognizer *)panGestureRecognizer {
    if([panGestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]){
        CGPoint velocity = [panGestureRecognizer velocityInView:self];
        return fabs(velocity.x) > fabs(velocity.y);
    }
    return NO;
}

// retrieve the table view from self
- (UITableView *)tableView {
    UIView *superView = self.superview;
    while (superView && ![superView isKindOfClass:[UITableView class]]) {
        superView = superView.superview;
    }
    
    if (superView) {
        return (UITableView *)superView;
    }
    
    return nil;
}

- (void)removePanGesture{
    [self.contentView removeGestureRecognizer:_panGesture];
}

- (void)addPanGesture{
    _panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panCell:)];
    _panGesture.delegate = self;
    [self.contentView addGestureRecognizer:_panGesture];
}
@end
