//
//  ChooseLocationCell.m
//  SnackFeed
//
//  Created by Daniel.Burke on 1/28/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import "ChooseLocationCell.h"

@implementation ChooseLocationCell

- (void)layoutSubviews{
    _selectionIcon.layer.cornerRadius = 10.f;
    _selectionIcon.layer.borderColor = DARK_GRAY.CGColor;
    _selectionIcon.layer.borderWidth = 1.f;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    _selectionIcon.backgroundColor = (selected) ? DARK_GRAY : LIGHT_GRAY;
    // Configure the view for the selected state
}

@end
