//
//  NomCell.h
//  SnackFeed
//
//  Created by Daniel.Burke on 1/23/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NomCell;

@protocol NomCellDelegate <NSObject>

@optional
- (void)setTableScrollEnabed:(BOOL)enabled;
- (void)didSwipeToPosition:(int)position;
- (void)cell:(NomCell*)cell atIndexPath:(NSIndexPath*)indexPath didPanToPosition:(CGFloat)position;

@end

@interface NomCell : UITableViewCell
<
UIGestureRecognizerDelegate
>

@property (nonatomic) BOOL snackIsGone;
@property (copy, nonatomic) UIPanGestureRecognizer *panGesture;
@property (strong, nonatomic) id <NomCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *actionView;
@property (weak, nonatomic) IBOutlet UILabel *statusNewIcon;
@property (weak, nonatomic) IBOutlet UILabel *statusAlmostIcon;
@property (weak, nonatomic) IBOutlet UILabel *statusGoneIcon;
@property (weak, nonatomic) IBOutlet UILabel *statusNewLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusAlmostLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusGoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *userActionLabel;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *actionTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *nomInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *nomDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *nomFloorLabel;
@property (weak, nonatomic) IBOutlet UILabel *nomSectionLabel;
@property (weak, nonatomic) IBOutlet UILabel *nomStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *nomStatusIcon;
@property (weak, nonatomic) IBOutlet UIButton *thankYouButton;
@property (weak, nonatomic) IBOutlet UIImageView *nomImageView;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;

- (void)removePanGesture;
- (void)addPanGesture;
@end
