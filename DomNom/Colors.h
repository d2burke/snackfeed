//
//  Colors.h
//  Transitions
//
//  Created by Daniel.Burke on 3/12/14.
//  Copyright (c) 2014 D2. All rights reserved.
//

#import <Foundation/Foundation.h>
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define DARK_GRAY UIColorFromRGB(0x666666)
#define GRAY UIColorFromRGB(0x9B9B9B)
#define MED_GRAY UIColorFromRGB(0xB2B2B2)
#define LIGHT_GRAY UIColorFromRGB(0xD0D0D0)
#define GOLD UIColorFromRGB(0xFFD52C)

#define FACEBOOK_BLUE UIColorFromRGB(0x4667AC)
#define TWITTER_BLUE UIColorFromRGB(0x22B2EF)
#define GOOGLE_RED UIColorFromRGB(0xDA4936)
#define PINTEREST_RED UIColorFromRGB(0xCB2028)
#define EMAIL_BLUE UIColorFromRGB(0x46b5ef)
#define MESSAGES_GREEN UIColorFromRGB(0x4fec43)

@interface Colors : NSObject

@end
