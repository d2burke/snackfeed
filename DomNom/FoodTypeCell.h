//
//  FoodTypeCell.h
//  SnackFeed
//
//  Created by Daniel.Burke on 2/12/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodTypeCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *foodTypeIcon;
@property (weak, nonatomic) IBOutlet UILabel *foodTypeLabel;
@end
