//
//  Utility.m
//  Occupied
//
//  Created by Daniel Burke on 1/21/14.
//  Copyright (c) 2014 Grow. All rights reserved.
//
/*
    
 Some common functions that mainly require the accessToken
 to work.  Initialize this class with the accessToken
 wherever you need to use it.
 
 */

#import <AssetsLibrary/AssetsLibrary.h>
#import "Utility.h"
#import "AFHTTPSessionManager.h"
#import "AFHTTPRequestOperationManager.h"

@implementation Utility

#pragma mark Networking
/**
 Make a post request with a dictionary of parameters to a given end point using AFNetworking. Method takes completion and failure callbacks for easy decision making upon completion
 Example usage:
 @code
 NSURL *url1, *url2;
 BOOL isNewer = [FileUtils
 isThisFileNewerThanThatFile:url1 thatURL:url2];
 @endcode
 @see http://www.dadabeatnik.com for more information.
 @param thisURL
 The URL of the source file.
 @param thatURL
 The URL of the target file to check.
 @return YES if the timestamp of @c thatURL is newer than the timestamp of @c thisURL,
 otherwise NO.
 */
- (void)   apiCall:(NSString *)endPoint
    withParameters:(NSDictionary *)params
        completion:(void (^)(id responseObject))completion
           failure:(void (^)(NSError *error))failed {
    endPoint =[endPoint stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/plain"];
	[manager.requestSerializer requestWithMethod:@"POST" URLString:endPoint parameters:params];
	[manager
	 POST:endPoint
	    parameters:params
	       success: ^(AFHTTPRequestOperation *operation, id responseObject) {
	    completion(responseObject);
	}  // success callback block

	       failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
	    failed(error);
	}];
}

- (void)   apiCall:(NSString *)endPoint
    withParameters:(NSDictionary *)params
         andMethod:(NSString *)method
           completion:(void (^)(AFHTTPRequestOperation *operation, id responseObject))completion
           failure:(void (^)(NSError *error))failed {
    endPoint =[endPoint stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
	manager.securityPolicy.allowInvalidCertificates = YES;
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/plain"];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
	[manager.requestSerializer requestWithMethod:method URLString:endPoint parameters:params];

	if ([method isEqualToString:@"GET"]) {
		[manager
		 GET:endPoint
		    parameters:params
		       success: ^(AFHTTPRequestOperation *operation, id responseObject) {
		    completion(operation, responseObject);
		}  // success callback block

		       failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
		    failed(error);
		}];
	}
	else {
		[manager
		 POST:endPoint
		    parameters:params
		       success: ^(AFHTTPRequestOperation *operation, id responseObject) {
		    completion(operation, responseObject);
		}  // success callback block

		       failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
		    failed(error);
		}];
	}
}

- (void)parseApiCall:(NSString *)endPoint
      withParameters:(NSDictionary *)params
          completion:(void (^)(id responseObject))completion
             failure:(void (^)(NSError *error))failed {
    endPoint =[endPoint stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/plain"];
    [manager.requestSerializer requestWithMethod:@"POST" URLString:endPoint parameters:params];
    [manager.requestSerializer setValue:@"JnkY7WGIRIEDXcVZ7ar41MFXh7TQHspSfpqw8WuT" forHTTPHeaderField:@"X-Parse-Application-Id"];
    [manager.requestSerializer setValue:@"wTipaNRbj3g3fFYASnZ0quqFapdm7vouyReu5uOu" forHTTPHeaderField:@"X-Parse-REST-API-Key"];
//    [manager.requestSerializer setValue:@"Content-Type" forHTTPHeaderField:@"application/json"];
    [manager
     POST:endPoint
	    parameters:params
     success: ^(AFHTTPRequestOperation *operation, id responseObject) {
         completion(responseObject);
     }  // success callback block
     
     failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"REsponse String: %@", operation.responseString);
         failed(error);
     }];
}

- (void)apiCallWithXML:(NSString *)endPoint
        withParameters:(NSDictionary *)params
            completion:(void (^)(id responseObject))completion
               failure:(void (^)(NSError *error))failed {
    endPoint =[endPoint stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
	manager.securityPolicy.allowInvalidCertificates = YES;
    manager.responseSerializer = [AFXMLParserResponseSerializer new];
	[manager.requestSerializer requestWithMethod:@"POST" URLString:endPoint parameters:params];
	[manager
	 POST:endPoint
     parameters:params
     success: ^(AFHTTPRequestOperation *operation, id responseObject) {
         completion(responseObject);
     }  // success callback block
     
     failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
         failed(error);
     }];
}

- (void)cancelAllCalls{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.operationQueue cancelAllOperations];
}

- (NSString *)nowString {
	NSDateFormatter *timeFormater = [[NSDateFormatter alloc] init];
	[timeFormater setDateFormat:@"hh:mm a"];
	NSDate *now = [NSDate date];
	NSString *nowStr = [timeFormater stringFromDate:now];
	return nowStr;
}

- (NSString *)readableDate:(NSDate *)date withType:(NSString *)type {
	NSDateFormatter *timeFormater = [[NSDateFormatter alloc] init];
	//TIME
	if ([type isEqualToString:@"time"]) {
		[timeFormater setDateFormat:@"hh:mm:ss a"];
	}
	//DAY
	else {
		[timeFormater setDateFormat:@"MMM/dd/yyyy hh:mm:ss a"];
	}
	NSString *readableDate = [timeFormater stringFromDate:date];
	return readableDate;
}

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
	NSString *timeString;
	NSInteger ti = (NSInteger)interval;
	NSInteger minutes = (ti / 60) % 60;
	NSInteger hours = (ti / 3600);
	if (!hours && !minutes) {
		timeString = @"1m";
	}
	else if (!hours) {
		timeString = [NSString stringWithFormat:@"%lim", (long)minutes];
	}
	else {
		timeString = [NSString stringWithFormat:@"%lihr",(long) hours];
	}
//	NSLog(@"Return time string: %@", timeString);
	return timeString;
}

- (UIImage *)imageWithColor:(UIColor *)color andRect:(CGRect)rect {
	rect.origin.y = 0;
	rect.origin.x = 0;
	UIGraphicsBeginImageContext(rect.size);
	CGContextRef context = UIGraphicsGetCurrentContext();

	CGContextSetFillColorWithColor(context, [color CGColor]);
	CGContextFillRect(context, rect);

	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();

	return image;
}

#pragma mark Image Picker Delegate
- (IBAction)choosePhoto:(id)sender {
    UIViewController *viewController = (UIViewController *)sender;
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [imagePicker setDelegate:self];
    
    [viewController presentViewController:imagePicker animated:YES completion:nil];
}

- (IBAction)takePhoto:(id)sender {
    UIViewController *viewController = (UIViewController *)sender;
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
    [imagePicker setDelegate:self];
    
    [viewController presentViewController:imagePicker animated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *) picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    __block NSString *imageFileName = nil;
    NSURL *imageURL = [info valueForKey:UIImagePickerControllerReferenceURL];
    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *myasset)
    {
        ALAssetRepresentation *representation = [myasset defaultRepresentation];
        NSString *fileName = [representation filename];
        imageFileName = fileName;
    };
    
    ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
    [assetslibrary assetForURL:imageURL
                   resultBlock:resultblock
                  failureBlock:nil];
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    [picker dismissViewControllerAnimated:YES completion:^(void){
        [_delegate didChoosePhoto:[NSDictionary dictionaryWithObjectsAndKeys:image, @"image", imageFileName, @"name", nil]];
    }];
}


- (NSString*)normalizedPriceString:(NSDictionary*)price{
    
    NSString *normalizedPrice;
    NSString *maxPrice = [price objectForKey:@"max"];
    NSString *minPrice = [price objectForKey:@"min"];
    NSString *amt = [price objectForKey:@"amt"];
    
    if(minPrice){
        NSCharacterSet *punctuation = [NSCharacterSet characterSetWithCharactersInString:@"$,"];
        maxPrice = [maxPrice stringByTrimmingCharactersInSet:punctuation];
        minPrice = [minPrice stringByTrimmingCharactersInSet:punctuation];
        
        if([maxPrice floatValue] == 0){
            normalizedPrice = @"Call for price";
        }
        else if([minPrice floatValue] == 0){
            normalizedPrice = [NSString stringWithFormat:@"$%@", maxPrice];
        }
        else if([maxPrice floatValue] == [minPrice floatValue]){
            normalizedPrice = [NSString stringWithFormat:@"$%@", minPrice];
        }
        else{
            normalizedPrice = [NSString stringWithFormat:@"$%@ - $%@", minPrice, maxPrice];
        }
    }
    else{
        normalizedPrice = amt;
    }
    return normalizedPrice;
}

@end
