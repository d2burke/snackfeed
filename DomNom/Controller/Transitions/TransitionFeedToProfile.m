//
//  TransitionFeedToProfile.m
//  SnackFeed
//
//  Created by Daniel.Burke on 2/8/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import "TransitionFeedToProfile.h"
#import "ProfileViewController.h"
#import "FeedViewController.h"

@implementation TransitionFeedToProfile

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    FeedViewController *fromViewController = (FeedViewController*)[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    ProfileViewController *toViewController = (ProfileViewController*)[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    toViewController.view.hidden = YES;
    
    UIView *containerView = [transitionContext containerView];
    NSTimeInterval duration = [self transitionDuration:transitionContext];
    
    [containerView addSubview:toViewController.view];
    
    [UIView animateWithDuration:duration delay:0 usingSpringWithDamping:0.75 initialSpringVelocity:0.25 options:UIViewAnimationOptionCurveEaseOut animations:^{
        fromViewController.blurView.frame = CGRectMake(0, 0, fromViewController.view.frame.size.width, fromViewController.view.frame.size.height);
        fromViewController.chooseLocationButtonView.alpha = 0;
    } completion:^(BOOL finished) {
        UIView *snapShot = [fromViewController.view snapshotViewAfterScreenUpdates:NO];
        snapShot.frame = [fromViewController.view convertRect:fromViewController.view.frame
                                                     fromView:fromViewController.view.superview];
        [containerView addSubview:snapShot];
        fromViewController.chooseLocationButtonView.alpha = 1;
        fromViewController.blurView.frame = CGRectMake(0, 0, fromViewController.view.frame.size.width, 40.f);
        [transitionContext completeTransition:!transitionContext.transitionWasCancelled];
    }];
}

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext{
    return .34;
}

@end
