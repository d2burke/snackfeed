//
//  TransitionProfileToFeed.m
//  SnackFeed
//
//  Created by Daniel.Burke on 2/8/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import "TransitionProfileToFeed.h"
#import "ProfileViewController.h"
#import "FeedViewController.h"

@implementation TransitionProfileToFeed

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    ProfileViewController *fromViewController = (ProfileViewController*)[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    FeedViewController *toViewController = (FeedViewController*)[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    UIView *containerView = [transitionContext containerView];
    NSTimeInterval duration = [self transitionDuration:transitionContext];
    
//    [containerView addSubview:toViewController.view];
    
    [UIView animateWithDuration:duration delay:0 usingSpringWithDamping:0.75 initialSpringVelocity:0.25 options:UIViewAnimationOptionCurveEaseOut animations:^{

    } completion:^(BOOL finished) {
        [transitionContext completeTransition:!transitionContext.transitionWasCancelled];
    }];
}

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext{
    return .34;
}


@end
