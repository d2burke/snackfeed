//
//  TransitionFeedToCreate.h
//  DomNom
//
//  Created by Daniel.Burke on 1/25/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TransitionFeedToCreate : NSObject <UIViewControllerAnimatedTransitioning>

@end
