//
//  TransitionFeedToCreate.m
//  DomNom
//
//  Created by Daniel.Burke on 1/25/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import "TransitionFeedToCreate.h"
#import "FeedViewController.h"
#import "CreateViewController.h"

@implementation TransitionFeedToCreate

-(void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext{
    
    FeedViewController *fromViewController = (FeedViewController*)[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    CreateViewController *toViewController = (CreateViewController*)[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    toViewController.view.hidden = YES;
    toViewController.createNomTableView.alpha = 0;
    fromViewController.blurView.hidden = YES;
    fromViewController.placeHolderView.hidden = YES;
    fromViewController.emptySetLabel.hidden = YES;
    
    UIView *containerView = [transitionContext containerView];
    NSTimeInterval duration = [self transitionDuration:transitionContext];
    
    UIView *snapShot = [fromViewController.placeHolderView snapshotViewAfterScreenUpdates:NO];
    snapShot.frame = [fromViewController.view convertRect:fromViewController.placeHolderView.frame fromView:fromViewController.placeHolderView.superview];
    
    CGRect originalTableFrame = fromViewController.availableFeedTableView.frame;
    CGRect feedTableFrame = fromViewController.availableFeedTableView.frame;
    feedTableFrame.origin.y = fromViewController.view.frame.size.height;
    
    [containerView addSubview:toViewController.view];
    [containerView addSubview:snapShot];
    
    [UIView animateWithDuration:duration/2 animations:^{
        snapShot.frame = CGRectMake(0, 64, toViewController.view.frame.size.width, 280.f);
    }];
    
    [UIView animateWithDuration:duration delay:0 usingSpringWithDamping:0.75 initialSpringVelocity:0.25 options:UIViewAnimationOptionCurveEaseOut animations:^{
        fromViewController.availableFeedTableView.frame = feedTableFrame;
    } completion:^(BOOL finished) {
        toViewController.view.hidden = NO;
        fromViewController.availableFeedTableView.frame = originalTableFrame;
        [transitionContext completeTransition:!transitionContext.transitionWasCancelled];
        
        [UIView animateWithDuration:0.34 animations:^{
            toViewController.createNomTableView.alpha = 1;
            fromViewController.blurView.hidden = NO;
            fromViewController.placeHolderView.hidden = NO;
            fromViewController.emptySetLabel.hidden = NO;
        } completion:^(BOOL finished) {
            [snapShot removeFromSuperview];
        }];
    }];
}

-(NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext{
    return .34;
}

@end
