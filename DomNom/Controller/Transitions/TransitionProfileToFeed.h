//
//  TransitionProfileToFeed.h
//  SnackFeed
//
//  Created by Daniel.Burke on 2/8/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TransitionProfileToFeed : NSObject <UIViewControllerAnimatedTransitioning>

@end
