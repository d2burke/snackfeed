//
//  UIImage+Color.h
//  Sift
//
//  Created by Daniel.Burke on 9/19/14.
//  Copyright (c) 2014 Dominion Enterprises. All rights reserved.
//

#import <Foundation/Foundation.h>

@import UIKit;

@interface UIImage (Color)

+ (UIImage *)imageWithColor:(UIColor *)color;

@end
