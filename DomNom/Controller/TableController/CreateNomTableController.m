//
//  CreateNomTableController.m
//  DomNom
//
//  Created by Daniel.Burke on 1/24/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import "CreateNomTableController.h"

@implementation CreateNomTableController

- (id)init{
    self = [super init];
    if(self){
        _floors = [[NSMutableArray alloc] init];
        _sections = [[NSMutableArray alloc] init];
    }
    return self;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{

}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    CGFloat y = -scrollView.contentOffset.y;
    if(y > 164){
        [_delegate didCancelCreateNom];
    }
}

#pragma mark - UITableView Delegate Methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [_delegate setValueAtIndex:(int)indexPath.row forSetting:_settingIndex];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  44;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch (_settingIndex) {
        case 0:
            return 3;
            break;
        case 1:
            return (int)[_floors count];
            break;
        case 2:
            return (int)[_sections count];
            break;
        default:
            return 0;
            break;
    }
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSString *stringTitle;
    switch (_settingIndex) {
        case 0:{
            NSArray *statuses = @[@"New",@"Almost Gone",@"Gone"];
            stringTitle = [statuses objectAtIndex:indexPath.row];
            break;
        }
        case 1:{
            TTTOrdinalNumberFormatter *ordinalNumberFormatter = [[TTTOrdinalNumberFormatter alloc] init];
            [ordinalNumberFormatter setLocale:[NSLocale currentLocale]];
            [ordinalNumberFormatter setGrammaticalGender:TTTOrdinalNumberFormatterMaleGender];
            stringTitle = [_floors objectAtIndex:indexPath.row];
            stringTitle = [NSString stringWithFormat:NSLocalizedString(@"%@ Floor", nil), [ordinalNumberFormatter stringFromNumber:[NSNumber numberWithInt:[stringTitle intValue]]]];
            break;
        }
        case 2:{
            stringTitle = [_sections objectAtIndex:indexPath.row];
            break;
        };
        default:
            return 0;
            break;
    }
    cell.textLabel.text = stringTitle;
    return cell;
}

@end
