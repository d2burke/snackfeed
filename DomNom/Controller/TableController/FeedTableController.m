//
//  FeedTableController.m
//  SnackFeed
//
//  Created by Daniel.Burke on 1/23/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//
#import "TTTTimeIntervalFormatter.h"
#import "FeedTableController.h"
#import "NomCell.h"

@implementation FeedTableController

- (id)init{
    self = [super init];
    if(self){
        _filteredSnacks = [[NSMutableArray alloc] init];
        _filteredSnacks = [[NSMutableArray alloc] init];
        _currentUser = [PFUser currentUser];
        [_currentUser fetchIfNeeded];
    }
    return self;
}

- (void)sayThankYou:(id)sender{
    UIButton *button = (UIButton*)sender;
    button.selected = !button.selected;
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_tableView];
    NSIndexPath *indexPath = [_tableView indexPathForRowAtPoint:buttonPosition];
    [_delegate sayThankYouForItemAtIndexPath:indexPath];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [_delegate tableViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    [_delegate tableViewDidEndDragging:scrollView];
}

#pragma mark - NomCellDelegate
- (void)setTableScrollEnabed:(BOOL)enabled{
    _tableView.scrollEnabled = enabled;
}

- (void)cell:(NomCell *)cell atIndexPath:(NSIndexPath *)indexPath didPanToPosition:(CGFloat)position{
    PFObject *item = [_filteredSnacks objectAtIndex:indexPath.row];
    if(position < 0){
        if(fabs(position) > _tableView.frame.size.width/2){
            [_delegate updateStatus:@"Gone" forItem:item];
        }
        else if(fabs(position) > _tableView.frame.size.width/4){
            [_delegate updateStatus:@"Almost Gone" forItem:item];
        }
    }
    else{
        if(position > _tableView.frame.size.width/2){

        }
        else if(position > _tableView.frame.size.width/4){
        }
    }
}
#pragma mark - UITableView Delegate Methods
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  280;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_filteredSnacks count];
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"nom";
    NomCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil){
        cell = [[NomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.delegate = self;
    
    PFObject *item = [_filteredSnacks objectAtIndex:indexPath.row];
    [item fetchIfNeeded];
    
    PFUser *user = item[@"User"];
    [user fetchIfNeeded];
    cell.usernameLabel.text = user.username;
    
    TTTTimeIntervalFormatter *timeIntervalFormatter = [[TTTTimeIntervalFormatter alloc] init];
    cell.actionTimeLabel.text = [timeIntervalFormatter stringForTimeIntervalFromDate:[NSDate date] toDate:item.createdAt];
    cell.userActionLabel.text = @"Daniel found some noms!";
    
    
    NSArray *statuses = @[@"New",@"Almost Gone",@"Gone"];
    NSArray *statusIcons = @[@"å",@"&",@"("];
    int iconIndex = (int)[statuses indexOfObject:item[@"Status"]];
    CGFloat iconFontSize = (iconIndex == 0) ? 14 : 18;
    UIColor *iconColor = [UIColor greenColor];
    switch (iconIndex) {
        case 1:
            iconColor = [UIColor orangeColor];
            break;
        case 2:
            iconColor = [UIColor redColor];
            break;
        default:
            break;
    }
    
    cell.nomInfoLabel.attributedText = nil;
    NSString *nomInfo = [NSString stringWithFormat:@"%@", [statusIcons objectAtIndex:iconIndex]];
    nomInfo = [nomInfo stringByAppendingString:[NSString stringWithFormat:@" %@", item[@"Status"]]];
    
    cell.snackIsGone = [item[@"Status"] isEqualToString:@"Gone"];
    
    TTTOrdinalNumberFormatter *ordinalNumberFormatter = [[TTTOrdinalNumberFormatter alloc] init];
    [ordinalNumberFormatter setLocale:[NSLocale currentLocale]];
    [ordinalNumberFormatter setGrammaticalGender:TTTOrdinalNumberFormatterMaleGender];
    NSString *floorString = item[@"Floor"];
    floorString = [NSString stringWithFormat:NSLocalizedString(@"%@ Floor", nil), [ordinalNumberFormatter stringFromNumber:[NSNumber numberWithInt:[floorString intValue]]]];
    nomInfo = [nomInfo stringByAppendingString:[NSString stringWithFormat:@" • %@", floorString]];
    if([item[@"Section"] length]){
        nomInfo = [nomInfo stringByAppendingString:[NSString stringWithFormat:@" • %@", item[@"Section"]]];
    }
    
    NSDictionary *normalFontDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                    cell.nomInfoLabel.font, NSFontAttributeName, nil];
    NSDictionary *iconFontDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [UIFont fontWithName:@"SnackFeed" size:iconFontSize], NSFontAttributeName,
                                  iconColor, NSForegroundColorAttributeName,
                                  @"-2", NSBaselineOffsetAttributeName,
                                  nil];
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:nomInfo attributes:normalFontDict];
    [aAttrString addAttributes:iconFontDict range:NSMakeRange(0, 1)];
    
    cell.nomInfoLabel.attributedText = aAttrString;
    cell.nomDescriptionLabel.text = item[@"Description"];
    PFFile *nomImage = [item[@"images"] lastObject];
    [nomImage getData];
    [cell.nomImageView setImageWithURL:[NSURL URLWithString:nomImage.url]];
    cell.thankYouButton.selected = [_currentUser[@"Thanks"] containsObject:item];
    [cell.thankYouButton addTarget:self action:@selector(sayThankYou:) forControlEvents:UIControlEventTouchUpInside];
    
    PFFile *userImage = user[@"image"];
    [userImage getData];
    [cell.userImageView setImageWithURL:[NSURL URLWithString:userImage.url]];
    return cell;
}

@end
