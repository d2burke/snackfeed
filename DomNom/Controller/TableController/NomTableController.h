//
//  NomTableController.h
//  DomNom
//
//  Created by Daniel.Burke on 1/23/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol NomTableControllerDelegate <NSObject>

@optional
- (void)tableViewDidScroll:(UIScrollView *)scrollView;
- (void)tableViewDidEndDecelerating:(UIScrollView *)scrollView;
- (void)tableViewDidEndDragging:(UIScrollView *)scrollView;
- (void)didDismissNom;

@end

@interface NomTableController : NSObject
<
UITableViewDelegate,
UITableViewDataSource,
UIScrollViewDelegate
>

@property (strong, nonatomic) NSMutableArray *activities;
@property (strong, nonatomic) id <NomTableControllerDelegate> delegate;

@end
