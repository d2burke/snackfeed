//
//  NomTableController.m
//  DomNom
//
//  Created by Daniel.Burke on 1/23/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import "TTTTimeIntervalFormatter.h"
#import "NomTableController.h"
#import "StatusActivityCell.h"
#import "CommentActivityCell.h"
#import "PhotoActivityCell.h"
#import "ThanksActivityCell.h"

@implementation NomTableController

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    [_delegate tableViewDidEndDragging:scrollView];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [_delegate tableViewDidScroll:scrollView];
}

#pragma mark - UITableView Delegate Methods
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    PFObject *activity = [_activities objectAtIndex:indexPath.row];
    [activity fetchIfNeeded];
    if([activity[@"Type"] isEqualToString:@"Photo"] ||
       [activity[@"Type"] isEqualToString:@"Status"]){
        return 300;
    }
    else if([activity[@"Type"] isEqualToString:@"Comment"]){
        
        /*
         There is a bug in iOS 7+ that makes it very difficult to calculate
         the height of a body of text within a textview until that textview
         has been rendered.  I'm still researching this and will replace
         this code with a better solution in the future.
         
         This allows iOS to determine the best frame to fit the given text
         at the given font and desired width.  We then use this to set the
         height of the cell for this row.
         */
        UITextView *textBubble = [[UITextView alloc] initWithFrame:CGRectMake(60, 0, 300, MAXFLOAT)];
        textBubble.editable = NO;
        textBubble.backgroundColor = [UIColor clearColor];
        textBubble.layer.cornerRadius = 14.f;
        textBubble.font = [UIFont systemFontOfSize:14.f];
        textBubble.text = [activity objectForKey:@"Text"];
        [textBubble sizeToFit];
        CGFloat cellHeight = (70 + textBubble.frame.size.height > 0) ? 70 + textBubble.frame.size.height : 70;
        NSLog(@"Cell Height: %f", cellHeight);
        return cellHeight;
    }

    return  70;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_activities count];
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"status";
    StatusActivityCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil){
        cell = [[StatusActivityCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    PFObject *activity = [_activities objectAtIndex:indexPath.row];
    [activity fetchIfNeeded];
    PFUser *user = activity[@"User"];
    [user fetchIfNeeded];
    
    if([activity[@"Type"] isEqualToString:@"Status"]){
        static NSString *CellIdentifier = @"photo";
        PhotoActivityCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil){
            cell = [[PhotoActivityCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        if(activity[@"Image"]){
            PFFile *activityImage = activity[@"Image"];
            [cell.photoImageView setImageWithURL:[NSURL URLWithString:activityImage.url]];
        }
        
        if(user[@"image"]){
            PFFile *userImage = user[@"image"];
            [cell.userImageView setImageWithURL:[NSURL URLWithString:userImage.url]];
        }
        cell.usernameLabel.text = user.username;
        
        TTTTimeIntervalFormatter *timeIntervalFormatter = [[TTTTimeIntervalFormatter alloc] init];
        cell.timeLabel.text = [timeIntervalFormatter stringForTimeIntervalFromDate:[NSDate date] toDate:activity.createdAt];
        cell.statusLabel.text = [NSString stringWithFormat:@"says this is %@", activity[@"Text"]];
        
        NSArray *statuses = @[@"New",@"Almost Gone",@"Gone"];
        NSArray *statusIcons = @[@"å",@"&",@"("];
        int iconIndex = (int)[statuses indexOfObject:activity[@"Text"]];
        cell.statusIcon.text = [statusIcons objectAtIndex:iconIndex];
        return cell;
    }
    else if([activity[@"Type"] isEqualToString:@"Comment"]){
        static NSString *CellIdentifier = @"comment";
        CommentActivityCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil){
            cell = [[CommentActivityCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        if(user[@"image"]){
            PFFile *userImage = user[@"image"];
            [cell.userImageView setImageWithURL:[NSURL URLWithString:userImage.url]];
        }
        cell.usernameLabel.text = user.username;
        
        TTTTimeIntervalFormatter *timeIntervalFormatter = [[TTTTimeIntervalFormatter alloc] init];
        cell.timeLabel.text = [timeIntervalFormatter stringForTimeIntervalFromDate:[NSDate date] toDate:activity.createdAt];
        cell.commentTextView.text = activity[@"Text"];
        return cell;
    }
    else if([activity[@"Type"] isEqualToString:@"Photo"]){
        static NSString *CellIdentifier = @"photo";
        PhotoActivityCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil){
            cell = [[PhotoActivityCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        if(activity[@"Image"]){
            PFFile *activityImage = activity[@"Image"];
            [cell.photoImageView setImageWithURL:[NSURL URLWithString:activityImage.url]];
        }
        
        if(user[@"image"]){
            PFFile *userImage = user[@"image"];
            [cell.userImageView setImageWithURL:[NSURL URLWithString:userImage.url]];
        }
        cell.usernameLabel.text = user.username;
        
        TTTTimeIntervalFormatter *timeIntervalFormatter = [[TTTTimeIntervalFormatter alloc] init];
        cell.timeLabel.text = [timeIntervalFormatter stringForTimeIntervalFromDate:[NSDate date] toDate:activity.createdAt];
        return cell;
    }
    else if([activity[@"Type"] isEqualToString:@"Thanks"]){
        static NSString *CellIdentifier = @"thanks";
        ThanksActivityCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil){
            cell = [[ThanksActivityCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        if(user[@"image"]){
            PFFile *userImage = user[@"image"];
            [cell.userImageView setImageWithURL:[NSURL URLWithString:userImage.url]];
        }
        
        cell.usernameLabel.text = user.username;
        
        TTTTimeIntervalFormatter *timeIntervalFormatter = [[TTTTimeIntervalFormatter alloc] init];
        cell.timeLabel.text = [timeIntervalFormatter stringForTimeIntervalFromDate:[NSDate date] toDate:activity.createdAt];
        return cell;
    }
    
    return cell;
}

@end
