//
//  CreateNomTableController.h
//  DomNom
//
//  Created by Daniel.Burke on 1/24/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol CreateNomTableControllerDelegate <NSObject>

@optional
- (void)setValueAtIndex:(int)index forSetting:(int)settingIndex;
- (void)didCancelCreateNom;

@end

@interface CreateNomTableController : NSObject
<
UITableViewDelegate,
UITableViewDataSource,
UIScrollViewDelegate
>

@property (nonatomic) int settingIndex;
@property (strong, nonatomic) NSMutableArray *floors;
@property (strong, nonatomic) NSMutableArray *sections;
@property (strong, nonatomic) id <CreateNomTableControllerDelegate> delegate;

@end
