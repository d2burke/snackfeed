//
//  ChooseLocationTableController.m
//  SnackFeed
//
//  Created by Daniel.Burke on 1/28/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import "ChooseLocationTableController.h"
#import "ChooseLocationCell.h"

@implementation ChooseLocationTableController

- (id)init{
    self = [super init];
    if(self){
        PFUser *currentUser = [PFUser currentUser];
        if(currentUser[@"FavoriteLocations"]){
            _favoriteLocations = [currentUser[@"FavoriteLocations"] mutableCopy];
        }
    }
    return self;
}

#pragma mark - UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [_delegate didSelectLocationAtIndex:indexPath];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if([_favoriteLocations count]){
        return 2;
    }
    return 1;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _tableView.frame.size.width, 25)];

    if([_favoriteLocations count]){
        headerView.backgroundColor = GOLD;
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, 200, 25)];
        titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:18.f];
        titleLabel.textColor = DARK_GRAY;
        [headerView addSubview:titleLabel];
        switch (section) {
            case 0:
                titleLabel.text = @"Favorite Locations";
                break;
            default:
                titleLabel.text = @"Suggestions";
                break;
        }
    }
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if([_favoriteLocations count]){
        return 25;
    }
    return 0.00001;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  50;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if([_favoriteLocations count]){
        switch (section) {
            case 0:
                return [_favoriteLocations count];
                break;
                
            default:
                return [_locations count];
                break;
        }
    }
    return [_locations count];
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"location";
    ChooseLocationCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil){
        cell = [[ChooseLocationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    PFObject *location;
    if([_favoriteLocations count]){
        switch (indexPath.section) {
            case 0:
                location = [_favoriteLocations objectAtIndex:indexPath.row];
                [location fetchIfNeeded];
                cell.nameLabel.text = location[@"Name"];
                cell.addressLabel.text = location[@"Address"];
                break;
                
            default:
                location = [_locations objectAtIndex:indexPath.row];
                cell.nameLabel.text = [location objectForKey:@"name"];
                cell.addressLabel.text = [location objectForKey:@"vicinity"];
                break;
        }
    }
    else{
        location = [_locations objectAtIndex:indexPath.row];
        cell.nameLabel.text = [location objectForKey:@"name"];
        cell.addressLabel.text = [location objectForKey:@"vicinity"];
    }
    
    return cell;
}
@end
