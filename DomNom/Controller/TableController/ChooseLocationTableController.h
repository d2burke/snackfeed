//
//  ChooseLocationTableController.h
//  SnackFeed
//
//  Created by Daniel.Burke on 1/28/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ChooseLocationTableControllerDelegate <NSObject>

@optional
- (void)didSelectLocationAtIndex:(NSIndexPath*)indexPath;

@end

@interface ChooseLocationTableController : NSObject
<
UITableViewDelegate,
UITableViewDataSource,
UIScrollViewDelegate
>

@property (strong, nonatomic) NSArray *locations;
@property (strong, nonatomic) NSMutableArray *favoriteLocations;
@property (strong, nonatomic) id <ChooseLocationTableControllerDelegate> delegate;
@property (strong, nonatomic) UITableView *tableView;

@end
