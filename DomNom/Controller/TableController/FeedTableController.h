//
//  FeedTableController.h
//  SnackFeed
//
//  Created by Daniel.Burke on 1/23/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "NomCell.h"

@protocol FeedTableControllerDelegate <NSObject>

@optional
- (void)tableViewDidScroll:(UIScrollView *)scrollView;
- (void)tableViewDidEndDecelerating:(UIScrollView *)scrollView;
- (void)tableViewDidEndDragging:(UIScrollView *)scrollView;
- (void)updateStatus:(NSString*)status forItem:(PFObject*)item;
- (void)sayThankYouForItemAtIndexPath:(NSIndexPath*)indexPath;
@end

@interface FeedTableController : NSObject
<
UITableViewDelegate,
UITableViewDataSource,
UIScrollViewDelegate,
NomCellDelegate
>
@property (strong, nonatomic) PFUser *currentUser;
@property (strong, nonatomic) NSMutableArray *noms;
@property (strong, nonatomic) NSMutableArray *filteredSnacks;
@property (strong, nonatomic) NSArray *favoriteLocations;
@property (strong, nonatomic) id <FeedTableControllerDelegate> delegate;

@property (strong, nonatomic) UITableView *tableView;

@end
