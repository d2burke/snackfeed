//
//  ChooseFilterTableController.h
//  SnackFeed
//
//  Created by Daniel.Burke on 2/10/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ChooseFilterTableControllerDelegate <NSObject>

@optional
- (void)didSelectItemAtIndex:(NSIndexPath*)indexPath;

@end

@interface ChooseFilterTableController : NSObject
<
UITableViewDelegate,
UIScrollViewDelegate,
UITableViewDataSource
>

@property (strong, nonatomic) NSArray *filters;
@property (strong, nonatomic) NSArray *filterIcons;
@property (strong, nonatomic) id <ChooseFilterTableControllerDelegate> delegate;
@property (strong, nonatomic) UITableView *tableView;


@end
