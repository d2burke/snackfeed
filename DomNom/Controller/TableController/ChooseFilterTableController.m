//
//  ChooseFilterTableController.m
//  SnackFeed
//
//  Created by Daniel.Burke on 2/10/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import "ChooseFilterTableController.h"
#import "ChooseFilterCell.h"

@implementation ChooseFilterTableController

- (id)init{
    self = [super init];
    if(self){
        _filters = @[@"New", @"Available", @"Almost Gone", @"Gone", @"All"];
        _filterIcons = @[@"å", @")", @"&", @"(", @"∂"];
    }
    return self;
}

#pragma mark - UITableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [_delegate didSelectItemAtIndex:indexPath];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_filters count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"filter";
    ChooseFilterCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil){
        cell = [[ChooseFilterCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.filterLabel.text = [_filters objectAtIndex:indexPath.row];
    cell.filterIcon.text = [_filterIcons objectAtIndex:indexPath.row];
    
    return cell;
}


@end
