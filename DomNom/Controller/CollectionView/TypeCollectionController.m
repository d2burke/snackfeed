//
//  TypeCollectionController.m
//  SnackFeed
//
//  Created by Daniel.Burke on 2/12/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import "TypeCollectionController.h"
#import "FoodTypeCell.h"

@implementation TypeCollectionController

- (instancetype)init{
    self = [super init];
    if(self){
        _types = [@[
                    @{@"type":@"Donuts",@"icon":@"∂"},
                    @{@"type":@"Pizza",@"icon":@"∞"},
                    @{@"type":@"Coffee",@"icon":@"7"},
                    @{@"type":@"Sandwiches",@"icon":@"¢"},
                    @{@"type":@"Candy",@"icon":@"÷"},
                    @{@"type":@"Cookies",@"icon":@"≥"},
                    @{@"type":@"Bagals",@"icon":@"≤"},
                    @{@"type":@"Cake",@"icon":@"µ"},
                    @{@"type":@"Fruit",@"icon":@"æ"},
                    @{@"type":@"Veggies",@"icon":@"…"},
                    @{@"type":@"Popcorn",@"icon":@"¬"},
                    @{@"type":@"Other",@"icon":@"?"}
                   ] mutableCopy];
    }
    return self;
}

#pragma mark UICollectionViewDelegate Methods
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//    UICollectionViewCell *cell = (UICollectionViewCell*)[collectionView cellForItemAtIndexPath:indexPath];
    [_delegate didSelectTypeAtIndexPath:indexPath];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_types count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    FoodTypeCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"type" forIndexPath:indexPath];
    cell.foodTypeIcon.text = [[_types objectAtIndex:indexPath.row] objectForKey:@"icon"];
    cell.foodTypeLabel.text = [[_types objectAtIndex:indexPath.row] objectForKey:@"type"];
    return cell;
}



@end
