//
//  TypeCollectionController.h
//  SnackFeed
//
//  Created by Daniel.Burke on 2/12/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TypeCollectionViewControllerDelegate <NSObject>

- (void)didSelectTypeAtIndexPath:(NSIndexPath*)indexPath;

@end

@interface TypeCollectionController : NSObject
<
UICollectionViewDataSource,
UICollectionViewDelegate,
UICollectionViewDelegateFlowLayout
>

@property (strong, nonatomic) NSMutableArray *types;
@property (strong, nonatomic) id <TypeCollectionViewControllerDelegate> delegate;

@property (strong, nonatomic) UICollectionView *collectionView;

@end
