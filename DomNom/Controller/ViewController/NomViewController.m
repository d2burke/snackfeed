//
//  NomViewController.m
//  SnackFeed
//
//  Created by Daniel.Burke on 1/23/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>
#import "UIImage+UIImage_fixOrientation_h.h"
#import "NomViewController.h"
#import "TTTTimeIntervalFormatter.h"

@interface NomViewController ()

@end

@implementation NomViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    _currentUser = [PFUser currentUser];
    _userImageView.layer.cornerRadius = 25.f;
    _nomTableController = [[NomTableController alloc] init];
    _nomTableController.delegate = self;
    _nomTableController.activities = [[NSMutableArray alloc] init];
    _nomActivityTableView.delegate = _nomTableController;
    _nomActivityTableView.dataSource = _nomTableController;
    
    [self getSnackActivities];
}

- (void)viewWillAppear:(BOOL)animated{    
    CGRect gradientFrame = _nomImageView.frame;
    UIColor *colorOne = [UIColor blackColor];
    UIColor *colorTwo = [[UIColor blackColor] colorWithAlphaComponent:0.7];
    NSArray *colors =  [NSArray arrayWithObjects:(id)colorOne.CGColor, colorTwo.CGColor, nil];
    NSNumber *stopOne = [NSNumber numberWithFloat:0.3];
    NSNumber *stopTwo = [NSNumber numberWithFloat:1];
    NSArray *locations = [NSArray arrayWithObjects:stopOne, stopTwo, nil];
    CAGradientLayer *maskLayer = [CAGradientLayer layer];
    maskLayer.frame = gradientFrame;
    maskLayer.locations = locations;
    maskLayer.colors = colors;
    _nomImageView.layer.mask = maskLayer;
    
    _nomInfoLabel.attributedText = nil;
    
    TTTTimeIntervalFormatter *timeIntervalFormatter = [[TTTTimeIntervalFormatter alloc] init];
    _actionTimeLabel.text = [timeIntervalFormatter stringForTimeIntervalFromDate:[NSDate date] toDate:_nom.createdAt];
    
    NSArray *statuses = @[@"New",@"Almost Gone",@"Gone"];
    NSArray *statusIcons = @[@"å",@"&",@"("];
    int iconIndex = (int)[statuses indexOfObject:_nom[@"Status"]];
    CGFloat iconFontSize = (iconIndex == 0) ? 14 : 18;
    UIColor *iconColor = [UIColor greenColor];
    switch (iconIndex) {
        case 1:
            iconColor = [UIColor orangeColor];
            break;
        case 2:
            iconColor = [UIColor redColor];
            break;
        default:
            break;
    }
    NSString *nomInfo = [NSString stringWithFormat:@"%@ ", [statusIcons objectAtIndex:iconIndex]];
    nomInfo = [nomInfo stringByAppendingString:[NSString stringWithFormat:@" %@", _nom[@"Status"]]];
    
    TTTOrdinalNumberFormatter *ordinalNumberFormatter = [[TTTOrdinalNumberFormatter alloc] init];
    [ordinalNumberFormatter setLocale:[NSLocale currentLocale]];
    [ordinalNumberFormatter setGrammaticalGender:TTTOrdinalNumberFormatterMaleGender];
    NSString *floorString = _nom[@"Floor"];
    floorString = [NSString stringWithFormat:NSLocalizedString(@"%@ Floor", nil), [ordinalNumberFormatter stringFromNumber:[NSNumber numberWithInt:[floorString intValue]]]];
    nomInfo = [nomInfo stringByAppendingString:[NSString stringWithFormat:@" • %@", floorString]];
    if([_nom[@"Section"] length]){
        nomInfo = [nomInfo stringByAppendingString:[NSString stringWithFormat:@" • %@", _nom[@"Section"]]];
    }
    
    NSDictionary *normalFontDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                    _nomInfoLabel.font, NSFontAttributeName, nil];
    NSDictionary *iconFontDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [UIFont fontWithName:@"SnackFeed" size:iconFontSize], NSFontAttributeName,
                                  iconColor, NSForegroundColorAttributeName,
                                  @"-2", NSBaselineOffsetAttributeName,
                                  nil];
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:nomInfo attributes:normalFontDict];
    [aAttrString addAttributes:iconFontDict range:NSMakeRange(0, 1)];
    
    _nomInfoLabel.attributedText = aAttrString;
    _nomDescriptionLabel.text = _nom[@"Description"];
    PFFile *nomImage = [_nom[@"images"] lastObject];
    [nomImage getData];
    [_nomImageView setImageWithURL:[NSURL URLWithString:nomImage.url]];
    
    [_commentField addTarget:self action:@selector(textFieldValueDidChange:) forControlEvents:UIControlEventValueChanged];
    
    _sayThankYouButton.selected = [_nom[@"Thanks"] containsObject:_currentUser];

    PFUser *user = _nom[@"User"];
    [user fetchIfNeeded];
    PFFile *userImage = user[@"image"];
    [userImage getData];
    [_userImageView setImageWithURL:[NSURL URLWithString:userImage.url]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextFieldDelegate
- (void)textFieldValueDidChange:(UITextField*)textField{
    [textField sizeToFit];
    [textField layoutIfNeeded];
}

#pragma SnackViewControllerDelegate
- (void)tableViewDidScroll:(UIScrollView *)scrollView{
    CGFloat y = -scrollView.contentOffset.y;
    if(y > 64){
        [_commentField resignFirstResponder];
    }
}

- (void)tableViewDidEndDragging:(UIScrollView *)scrollView{
    CGFloat y = -scrollView.contentOffset.y;
    
    if(y > 164){
        [self didDismissNom];
    }
}

#pragma mark - Keyboard Methods
-(void)keyboardWillShow:(NSNotification*)notification{
    [self.view layoutIfNeeded];
    // keyboard frame is in window coordinates
    NSDictionary *userInfo = [notification userInfo];
    CGRect keyboardFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    _commentBarBottomContstraint.constant = keyboardFrame.size.height;
    UIEdgeInsets formInset = _nomActivityTableView.contentInset;
    formInset.bottom = keyboardFrame.size.height - 220.f;
    [UIView animateWithDuration:0.34 animations:^{
        [self.view layoutIfNeeded];
        _nomActivityTableView.contentInset = formInset;
    }];
}

-(void)keyboardWillHide:(NSNotification*)notification{
    [self.view layoutIfNeeded];
    _commentBarBottomContstraint.constant = 0;
    UIEdgeInsets formInset = _nomActivityTableView.contentInset;
    formInset.bottom = 0;
    [UIView animateWithDuration:0.34 animations:^{
        [self.view layoutIfNeeded];
        _nomActivityTableView.contentInset = formInset;
    } completion:^(BOOL finished) {
        
    }];
}


#pragma mark - ImagePickerDelegate Methods
- (IBAction)takePhoto:(id)sender{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
    [imagePicker setDelegate:self];
    [self presentViewController:imagePicker animated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *) picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    __block NSString *imageFileName = nil;
    NSURL *imageURL = [info valueForKey:UIImagePickerControllerReferenceURL];
    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *myasset)
    {
        ALAssetRepresentation *representation = [myasset defaultRepresentation];
        NSString *fileName = [representation filename];
        imageFileName = fileName;
    };
    
    ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
    [assetslibrary assetForURL:imageURL
                   resultBlock:resultblock
                  failureBlock:nil];
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    image = [image fixOrientation];
    
    float oldWidth = image.size.width;
    float scaleFactor = 1280.f / oldWidth;
    
    float newHeight = image.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [image drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    _imageData = UIImageJPEGRepresentation(newImage, 0.0);
    
    
    [self dismissViewControllerAnimated:YES completion:^(void){
        [self sendCommentImage:newImage];
    }];
}

#pragma mark - NomTableControllerDelegate
- (void)didDismissNom{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Custom

- (void)getSnackActivities{
    PFQuery *query = [PFQuery queryWithClassName:@"Activity"];
    [query whereKey:@"Snack" equalTo:_nom];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(!error){
            _nomTableController.activities = [objects mutableCopy];
            [_nomActivityTableView reloadData];
        }
        else{
            NSLog(@"Activity Error: %@", error);
        }
    }];
}

- (IBAction)sayThankYou:(id)sender{
    UIButton *button = (UIButton*)sender;
    if([_currentUser[@"Thanks"] containsObject:_nom]){
        button.selected = NO;
        PFQuery *deleteQuery = [PFQuery queryWithClassName:@"Activity"];
        [deleteQuery whereKey:@"User" equalTo:_currentUser];
        [deleteQuery whereKey:@"Snack" equalTo:_nom];
        [deleteQuery whereKey:@"Type" equalTo:@"Thanks"];
        [deleteQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            [_nomTableController.activities removeObjectsInArray:objects];
            [PFObject deleteAllInBackground:objects];
            [_nomActivityTableView reloadData];
        }];
        
        [_currentUser[@"Thanks"] removeObject:_nom];
        [_currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            [_nom[@"Thanks"] removeObject:_currentUser];
            [_nom saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                NSLog(@"Removed");
            }];
        }];
    }
    else{
        button.selected = YES;
        PFObject *activity = [PFObject objectWithClassName:@"Activity"];
        activity[@"User"] = _currentUser;
        activity[@"Snack"] = _nom;
        activity[@"Type"] = @"Thanks";
        [activity saveInBackground];
        [_nomTableController.activities addObject:activity];
        
        [_currentUser addUniqueObject:_nom forKey:@"Thanks"];
        [_currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            [_nom addUniqueObject:_currentUser forKey:@"Thanks"];
            [_nom saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                [_nomActivityTableView reloadData];
                NSLog(@"Added");
            }];
        }];
    }
}

- (IBAction)sendComment:(id)sender {
    if([_commentField.text length]){
        //Accept auto-correct suggestion
        [_commentField resignFirstResponder];
        [_commentField becomeFirstResponder];
        
        PFObject *activity = [PFObject objectWithClassName:@"Activity"];
        activity[@"User"] = _currentUser;
        activity[@"Snack"] = _nom;
        activity[@"Type"] = @"Comment";
        activity[@"Text"] = _commentField.text;
        
        [_nomTableController.activities addObject:activity];
        [_nomActivityTableView reloadData];
        
        _commentField.text = @"";
        [_commentField resignFirstResponder];
        
        [activity saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        }];
    }
}

- (void)sendCommentImage:(UIImage*)image{
    PFFile *imageFile = [PFFile fileWithData:_imageData];
    [imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        PFObject *activity = [PFObject objectWithClassName:@"Activity"];
        activity[@"User"] = _currentUser;
        activity[@"Snack"] = _nom;
        activity[@"Type"] = @"Photo";
        activity[@"Image"] = imageFile;
        [_nomTableController.activities addObject:activity];
        [activity saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            NSLog(@"Image Saved");
            [_nomActivityTableView reloadData];
        }];
    }];
}

@end
