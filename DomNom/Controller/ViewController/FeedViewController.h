//
//  FeedViewController.h
//  SnackFeed
//
//  Created by Daniel.Burke on 1/23/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "FeedTableController.h"
#import "CreateViewController.h"
#import "NomViewController.h"
#import "OnboardingViewController.h"
#import "ChooseLocationTableController.h"
#import "ChooseFilterTableController.h"

typedef enum {
    StatusNew = 1,
    StatusAvailable,
    StatusAlmostGone,
    StatusGone,
    STatusAll
} Status;

@interface FeedViewController : UIViewController
<CLLocationManagerDelegate,
UINavigationControllerDelegate,
UIImagePickerControllerDelegate,
ChooseLocationTableControllerDelegate,
ChooseFilterTableControllerDelegate,
OnboardingViewControllerDelegate,
FeedTableControllerDelegate,
CreateNomDelegate
>

@property (strong, nonatomic) NSString *feedFilterStatus;
@property (strong, nonatomic) Utility *utility;
@property (strong, nonatomic) NSData *imageData;
@property (strong, nonatomic) NSString *currentOfficeId;
@property (strong, nonatomic) NSString *selectedStatus;
@property (strong, nonatomic) PFUser *currentUser;
@property (strong, nonatomic) PFObject *selectedLocation;
@property (strong, nonatomic) PFObject *selectedSnack;
@property (strong, nonatomic) FeedTableController *feedTableController;
@property (strong, nonatomic) NSMutableArray *noms;
@property (strong, nonatomic) NSArray *locations;
@property (strong, nonatomic) NSMutableArray *favoriteLocations;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation *currentLocation;
@property (strong, nonatomic) ChooseLocationTableController *chooseLocationTableController;
@property (strong, nonatomic) ChooseFilterTableController *chooseFilterTableController;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *switchLocationTapGesture;

@property (weak, nonatomic) IBOutlet UIView *chooseFilterView;
@property (weak, nonatomic) IBOutlet UIView *chooseLocationView;
@property (weak, nonatomic) IBOutlet UIView *chooseLocationButtonView;
@property (weak, nonatomic) IBOutlet UILabel *emptySetLabel;
@property (weak, nonatomic) IBOutlet UILabel *logo;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *selectedLocationLabel;
@property (weak, nonatomic) IBOutlet UIView *placeHolderView;
@property (weak, nonatomic) IBOutlet UIVisualEffectView *blurView;
@property (weak, nonatomic) IBOutlet UIButton *userProfileButton;
@property (weak, nonatomic) IBOutlet UIButton *filterFeedButton;
@property (weak, nonatomic) IBOutlet UIButton *setFavoriteLocationButton;
@property (weak, nonatomic) IBOutlet UIButton *placeHolderStatusButton;
@property (weak, nonatomic) IBOutlet UIButton *placeHolderFloorButton;
@property (weak, nonatomic) IBOutlet UIButton *placeHolderSectionButton;
@property (weak, nonatomic) IBOutlet UIScrollView *feedScrollView;
@property (weak, nonatomic) IBOutlet UITableView *availableFeedTableView;
@property (weak, nonatomic) IBOutlet UITableView *chooseLocationTableView;
@property (weak, nonatomic) IBOutlet UITableView *chooseFilterTableView;

- (IBAction)hideModals:(id)sender;
- (IBAction)switchLocation:(id)sender;
- (IBAction)setFavoriteLocation:(id)sender;
- (IBAction)displayFeedFilters:(id)sender;
- (IBAction)hideFeedFilters:(id)sender;
@end
