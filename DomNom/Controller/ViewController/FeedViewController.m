//
//  FeedViewController.m
//  SnackFeed
//
//  Created by Daniel.Burke on 1/23/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>
#import "FeedViewController.h"
#import "NomViewController.h"
#import "ProfileViewController.h"
#import "TransitionFeedToCreate.h"
#import "TransitionFeedToProfile.h"
#import "ChooseLocationCell.h"

@interface FeedViewController ()

@end

@implementation FeedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _feedScrollView.scrollsToTop = NO;
    _utility = [[Utility alloc] init];
    _locations = [[NSArray alloc] init];
    _feedTableController = [[FeedTableController alloc] init];
    _feedTableController.delegate = self;
    _feedTableController.tableView = _availableFeedTableView;
    _availableFeedTableView.delegate = _feedTableController;
    _availableFeedTableView.dataSource = _feedTableController;
    
    _currentUser = [PFUser currentUser];
    _favoriteLocations = _currentUser[@"FavoriteLocations"];
    _feedFilterStatus = @"Available";
    
    _placeHolderStatusButton.layer.cornerRadius = 6.f;
    _placeHolderSectionButton.layer.cornerRadius = 6.f;
    _placeHolderFloorButton.layer.cornerRadius = 6.f;
    _chooseLocationView.layer.cornerRadius = 10.f;
    _chooseFilterView.layer.cornerRadius = 10.f;
    _userProfileButton.layer.cornerRadius = 16.5f;
    _userProfileButton.layer.borderColor = DARK_GRAY.CGColor;
    _userProfileButton.layer.borderWidth = 2.f;
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"DidCompleteOnboarding"]){
        //Check for a chosen location
        NSString *officeId = [[NSUserDefaults standardUserDefaults] objectForKey:@"SelectedLocationId"];
        if(officeId){
            _currentOfficeId = officeId;
        }
        _selectedLocation = [PFObject objectWithoutDataWithClassName:@"Office" objectId:_currentOfficeId];
        [_selectedLocation fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            _selectedLocation = object;
            _selectedLocationLabel.text = _selectedLocation[@"Name"];
            _setFavoriteLocationButton.selected = [[[PFUser currentUser] objectForKey:@"FavoriteLocations"] containsObject:_selectedLocation];
            [self getSnacksForOffice:_selectedLocation];
        }];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationController.delegate = self;
    _availableFeedTableView.contentInset = UIEdgeInsetsMake(40, 0, 0, 0);
    
    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"DidCompleteOnboarding"] &&
       ![[NSUserDefaults standardUserDefaults] objectForKey:@"DidSkipOnboarding"]){
        [self performSegueWithIdentifier:@"FeedToOnboarding" sender:self];
    }
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"DidAllowLocation"]){
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.delegate = self;
        
        if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
            [_locationManager requestWhenInUseAuthorization];
        }
    }
}

- (void)viewDidAppear:(BOOL)animated{
    _chooseLocationTableController = [[ChooseLocationTableController alloc] init];
    _chooseLocationTableController.tableView = _chooseLocationTableView;
    _chooseLocationTableController.delegate = self;
    _chooseLocationTableController.locations = _locations;
    _chooseLocationTableController.favoriteLocations = _favoriteLocations;
    _chooseLocationTableView.delegate = _chooseLocationTableController;
    _chooseLocationTableView.dataSource  = _chooseLocationTableController;
    
    _chooseFilterTableController = [[ChooseFilterTableController alloc] init];
    _chooseFilterTableController.tableView = _chooseFilterTableView;
    _chooseFilterTableController.delegate = self;
    _chooseFilterTableView.delegate = _chooseFilterTableController;
    _chooseFilterTableView.dataSource = _chooseFilterTableController;
    [_chooseFilterTableView reloadData];
}

- (void)viewDidDisappear:(BOOL)animated{
    self.navigationController.delegate = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"FeedToNom"]){
        NomViewController *nomViewController = (NomViewController*)[segue destinationViewController];
        nomViewController.nom = [_feedTableController.filteredSnacks objectAtIndex:_availableFeedTableView.indexPathForSelectedRow.row];
    }
    else if([segue.identifier isEqualToString:@"FeedToCreate"]){
        CreateViewController *createViewController = (CreateViewController*)[segue destinationViewController];
        createViewController.selectedLocation = _selectedLocation;
        createViewController.delegate = self;
    }
    else if([segue.identifier isEqualToString:@"FeedToOnboarding"]){
        UINavigationController *navController = (UINavigationController*)[segue destinationViewController];
        OnboardingViewController *onboardingViewController = (OnboardingViewController*)[navController viewControllers][0];
        onboardingViewController.delegate = self;
    }
}


#pragma mark - UINavigationController Delegate Methods
- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                  animationControllerForOperation:(UINavigationControllerOperation)operation
                                               fromViewController:(UIViewController *)fromVC
                                                 toViewController:(UIViewController *)toVC {

    if (fromVC == self && [toVC isKindOfClass:[CreateViewController class]]) {
        return [[TransitionFeedToCreate alloc] init];
    }
    if (fromVC == self && [toVC isKindOfClass:[ProfileViewController class]]) {
        return [[TransitionFeedToProfile alloc] init];
    }
    else {
        return nil;
    }
}

#pragma mark - OnboardingViewControllerDelegate
- (void)didCompleteOnboardingAtLocation:(PFObject *)location{
    _currentOfficeId = location.objectId;
    _selectedLocation = location;
    _selectedLocationLabel.text = _selectedLocation[@"Name"];
    [self getSnacksForOffice:location];
    [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"DidCompleteOnboarding"];
    [self dismissViewControllerAnimated:YES completion:^{
        //
    }];
}

- (void)didCompleteOnboarding{
    [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"DidCompleteOnboarding"];
    [self dismissViewControllerAnimated:YES completion:^{
        //
    }];
}

- (void)didSkipOnboarding{
    //Check for a chosen location
    NSString *officeId = [[NSUserDefaults standardUserDefaults] objectForKey:@"SelectedLocationId"];
    [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"DidSkipOnboarding"];
    [self dismissViewControllerAnimated:YES completion:^{
        //
    }];
}

#pragma mark - CreateNomDelegate
- (void)didCreateNom:(PFObject *)nom{
    [_feedTableController.filteredSnacks addObject:nom];
    
    //Resort and display contact list
    dispatch_queue_t hideQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(hideQueue, ^{
        NSSortDescriptor *dateSort = [[NSSortDescriptor alloc] initWithKey:@"createdAt" ascending:NO];
        [_feedTableController.noms sortUsingDescriptors:@[dateSort]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [_availableFeedTableView reloadData];
        });
    });
    NSLog(@"Display Feed");
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - FeedTableControllerDelegate
- (void)tableViewDidScroll:(UIScrollView *)scrollView{
    CGFloat y = -scrollView.contentOffset.y - _availableFeedTableView.contentInset.top;
    
    CGRect placeHolderFrame = _placeHolderView.frame;
    placeHolderFrame.origin.y = (y < 240) ? -240 + y : 0;
    _placeHolderView.frame = placeHolderFrame;
    _placeHolderView.alpha = y/(self.view.frame.size.height/3);
    
    CGRect blurFrame = _blurView.frame;
    blurFrame.origin.y = (y > 0) ? -y : 0;
    _blurView.frame = blurFrame;
    
    CGRect logoFrame = _logo.frame;
    logoFrame.origin.y = (y > 0) ? (y/self.view.frame.size.height) * 90 : 0;
    _logo.frame = logoFrame;
    _logo.alpha = 1 - ((y/self.view.frame.size.height) * 30);
    _emptySetLabel.alpha = 1 - ((y/self.view.frame.size.height) * 30);
    _statusLabel.alpha = ((y/self.view.frame.size.height) * 30);
    
    if(y > self.view.frame.size.height/7){
        _statusLabel.text = @"Release to Create";
    }
    else if(y > self.view.frame.size.height/9){
        _statusLabel.text = @"Pull to Create";
    }
    else if(y > self.view.frame.size.height/15){
        _statusLabel.text = @"Release to Refresh";
    }
    else{
        _statusLabel.text = @"Pull to Refresh";
    }
}

- (void)tableViewDidEndDragging:(UIScrollView *)scrollView{
    CGFloat y = -scrollView.contentOffset.y - _availableFeedTableView.contentInset.top;
    if(y > self.view.frame.size.height/7){
        [self performSegueWithIdentifier:@"FeedToCreate" sender:self];
    }
    else if(y < self.view.frame.size.height/9 && y > self.view.frame.size.height/15){
        [self getSnacksForOffice:_selectedLocation];
    }
}

- (void)updateStatus:(NSString *)status forItem:(PFObject *)item{
    _selectedSnack = item;
    _selectedStatus = status;
    [self takePhoto:self];
}

- (void)sayThankYouForItemAtIndexPath:(NSIndexPath *)indexPath{
    PFObject *item = [_feedTableController.filteredSnacks objectAtIndex:indexPath.row];
    
    if([_currentUser[@"Thanks"] containsObject:item]){
        PFQuery *deleteQuery = [PFQuery queryWithClassName:@"Activity"];
        [deleteQuery whereKey:@"User" equalTo:_currentUser];
        [deleteQuery whereKey:@"Snack" equalTo:item];
        [deleteQuery whereKey:@"Type" equalTo:@"Thanks"];
        [deleteQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            [PFObject deleteAllInBackground:objects];
        }];
        
        [_currentUser[@"Thanks"] removeObject:item];
        [_currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            [item[@"Thanks"] removeObject:_currentUser];
            [item saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                [_availableFeedTableView reloadData];
                NSLog(@"Removed");
            }];
        }];
    }
    else{
        PFObject *activity = [PFObject objectWithClassName:@"Activity"];
        activity[@"User"] = _currentUser;
        activity[@"Snack"] = item;
        activity[@"Type"] = @"Thanks";
        [activity saveInBackground];
        
        [_currentUser addUniqueObject:item forKey:@"Thanks"];
        [_currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            [item addUniqueObject:_currentUser forKey:@"Thanks"];
            [item saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                [_availableFeedTableView reloadData];
                NSLog(@"Added");
            }];
        }];
    }
}

#pragma mark - ImagePickerDelegate Methods
- (IBAction)takePhoto:(id)sender{
    if(_selectedSnack){
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
        [imagePicker setDelegate:self];
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}

-(void)imagePickerController:(UIImagePickerController *) picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    __block NSString *imageFileName = nil;
    NSURL *imageURL = [info valueForKey:UIImagePickerControllerReferenceURL];
    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *myasset)
    {
        ALAssetRepresentation *representation = [myasset defaultRepresentation];
        NSString *fileName = [representation filename];
        imageFileName = fileName;
    };
    
    ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
    [assetslibrary assetForURL:imageURL
                   resultBlock:resultblock
                  failureBlock:nil];
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    image = [image fixOrientation];
    
    float oldWidth = image.size.width;
    float scaleFactor = 1280.f / oldWidth;
    
    float newHeight = image.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [image drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    _imageData = UIImageJPEGRepresentation(newImage, 0.0);
    
    PFFile *imageFile = [PFFile fileWithData:_imageData];
    [imageFile saveInBackground];
    _selectedSnack[@"Status"] = _selectedStatus;
    [_selectedSnack addUniqueObject:imageFile forKey:@"images"];
    [self filterFeedByStatus:_feedFilterStatus];
    
    [self dismissViewControllerAnimated:YES completion:^(void){
        [_selectedSnack saveInBackground];
        
        PFObject *activity = [PFObject objectWithClassName:@"Activity"];
        activity[@"User"] = _currentUser;
        activity[@"Snack"] = _selectedSnack;
        activity[@"Type"] = @"Status";
        activity[@"Text"] = _selectedSnack[@"Status"];
        activity[@"Image"] = imageFile;
        [activity saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            NSLog(@"Status Saved");
        }];
        
        //Trigger push for location about this item
        [PFCloud callFunctionInBackground:@"sendItemStatusNotification"
                           withParameters:@{@"Status":_selectedSnack[@"Status"] ,@"ItemId":_selectedSnack.objectId}
                                    block:^(id response, NSError *error) {
                                        if (error) {
                                            NSLog(@"Push Error: %@", error);
                                        }
                                        else{
                                            NSLog(@"Push Status; %@", response);
                                        }
                                    }];
        _selectedSnack = nil;
    }];
}

#pragma mark - ChooseLocationTableControllerDelegate
- (void)didSelectLocationAtIndex:(NSIndexPath *)indexPath{
    if([_favoriteLocations count] && (int)indexPath.section == 0){
        NSLog(@"Favorite Location");
        _selectedLocation = [_favoriteLocations objectAtIndex:indexPath.row];
        _setFavoriteLocationButton.selected = [[[PFUser currentUser] objectForKey:@"FavoriteLocations"] containsObject:_selectedLocation];
        [_selectedLocation fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            [[NSUserDefaults standardUserDefaults] setObject:object.objectId forKey:@"SelectedLocationId"];
        }];
        
        //Get snack for location
        [self getSnacksForOffice:_selectedLocation];
        
        NSString *channelName = [NSString stringWithFormat:@"office-%@", _selectedLocation.objectId];
        PFInstallation *currentInstallation = [PFInstallation currentInstallation];
        [currentInstallation addUniqueObject:channelName forKey:@"channels"];
        [currentInstallation saveInBackground];
        
        ChooseLocationCell *cell = (ChooseLocationCell*)[_chooseLocationTableView cellForRowAtIndexPath:indexPath];
        _selectedLocationLabel.text = cell.nameLabel.text;
        [self setLocationsHidden:YES];
    }
    else{
        NSDictionary *location = [_locations objectAtIndex:indexPath.row];
        _currentLocation = [[CLLocation alloc] initWithLatitude:[[[[location objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lat"] floatValue] longitude:[[[[location objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lat"] floatValue]];
        
        //Check to see if a location exists, if it doesn't create it and return the object
        [PFCloud callFunctionInBackground:@"upsertLocation" withParameters:location block:^(id object, NSError *error) {
            if(!error){
                //Subscribe user to notifications for this location
                _selectedLocation = [object objectForKey:@"Location"];
                _setFavoriteLocationButton.selected = [[[PFUser currentUser] objectForKey:@"FavoriteLocations"] containsObject:_selectedLocation];
                [_selectedLocation fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                    [[NSUserDefaults standardUserDefaults] setObject:object.objectId forKey:@"SelectedLocationId"];
                }];
                
                //Get snack for location
                [self getSnacksForOffice:_selectedLocation];
                
                NSString *channelName = [NSString stringWithFormat:@"office-%@", _selectedLocation.objectId];
                PFInstallation *currentInstallation = [PFInstallation currentInstallation];
                [currentInstallation addUniqueObject:channelName forKey:@"channels"];
                [currentInstallation saveInBackground];
                
                ChooseLocationCell *cell = (ChooseLocationCell*)[_chooseLocationTableView cellForRowAtIndexPath:indexPath];
                _selectedLocationLabel.text = cell.nameLabel.text;
                [self setLocationsHidden:YES];
                
                NSMutableArray *snacks = [object objectForKey:@"Snacks"];
                if([snacks count]){
                    NSLog(@"Found Snacks!: %@", snacks);
                }
            }
            else{
                NSLog(@"Upsert Error: %@", error);
            }
        }];
    }
}

#pragma mark - ChooseFilterTableControllerDelegate
- (void)didSelectItemAtIndex:(NSIndexPath *)indexPath{
    Status status = (Status)indexPath.row+1;
    switch (status) {
        case StatusNew:{
            _feedFilterStatus = @"New";
            break;
        }
        case StatusAvailable:{
            _feedFilterStatus = @"Available";
            break;
        }
        case StatusAlmostGone:{
            _feedFilterStatus = @"Almost Gone";
            break;
        }
        case StatusGone:{
            _feedFilterStatus = @"Gone";
            break;
        }
        default:{
            _feedFilterStatus = nil; //All
            break;
        }
    }
    
    [self filterFeedByStatus:_feedFilterStatus];
}


#pragma mark - CLLocation Delegate Methods
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    if(status != kCLAuthorizationStatusAuthorizedWhenInUse){
        NSLog(@"Throw Error and explain benefits of location");
    }
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [_locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation{
    if(newLocation != _currentLocation){
        _currentLocation = newLocation;
        NSLog(@"Location: %@", _currentLocation);
        [_locationManager stopUpdatingLocation];
        [self getNearbyLocations];
    }
    else{
        NSLog(@"Same Location");
    }
}

#pragma mark - Custom

- (void)getNearbyLocations{
    NSString *endPoint = [NSString stringWithFormat:
                          @"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%f,%f&radius=500&rankBy=distance&key=AIzaSyBMQgDdx1cDt5-z9q4oygHGRmPweGSq73A", _currentLocation.coordinate.latitude, _currentLocation.coordinate.longitude];
    
    [_utility apiCall:endPoint withParameters:nil andMethod:@"GET" completion:^(AFHTTPRequestOperation *operation, id responseObject) {
        _locations = [responseObject objectForKey:@"results"];
        _chooseLocationTableController.locations = _locations;
        [_chooseLocationTableView reloadData];
        NSLog(@"Got New Predictions");
    } failure:^(NSError *error) {
        //
    }];
}

- (void)getSnacksForOffice:(PFObject*)office{
    PFQuery *query = [PFQuery queryWithClassName:@"Nom"];
    [query whereKey:@"Office" equalTo:office];
    [query orderByDescending:@"createdAt"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *snacks, NSError *error) {
        if (!error) {
            _emptySetLabel.hidden = [snacks count];
            _feedTableController.noms = [snacks mutableCopy];
            _feedTableController.filteredSnacks = [snacks mutableCopy];
            [self filterFeedByStatus:_feedFilterStatus];
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
}

- (void)setLocationsHidden:(BOOL)yes{
    if(yes){
        _blurView.frame = CGRectMake(0, 0, self.view.frame.size.width, 40);
        [UIView animateWithDuration:0.2 animations:^{
            _chooseLocationView.alpha = 0;
        } completion:^(BOOL finished) {
            _chooseLocationView.hidden = YES;
        }];
    }
    else{
        _chooseLocationView.hidden = NO;
        _blurView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        [UIView animateWithDuration:0.2 animations:^{
            _chooseLocationView.alpha = 1;
        }];
    }
}

- (IBAction)switchLocation:(id)sender {
    [self setLocationsHidden:!_chooseLocationView.hidden];
}

- (IBAction)setFavoriteLocation:(id)sender {
    UIButton *button = (UIButton*)sender;
    PFUser *currentUser = [PFUser currentUser];
    if(button.selected){
        [currentUser removeObject:_selectedLocation forKey:@"FavoriteLocations"];
        [currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            button.selected = NO;
        }];
        [_chooseLocationTableController.favoriteLocations removeObject:_selectedLocation];
    }
    else{
        [currentUser addUniqueObject:_selectedLocation forKey:@"FavoriteLocations"];
        [currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            button.selected = YES;
        }];
        [_chooseLocationTableController.favoriteLocations addObject:_selectedLocation];
    }
    _favoriteLocations = currentUser[@"FavoriteLocations"];
    _chooseLocationTableController.favoriteLocations = _favoriteLocations;
    [_chooseLocationTableView reloadData];
}

- (IBAction)displayFeedFilters:(id)sender {
    _chooseFilterView.hidden = NO;
    _chooseLocationButtonView.hidden = YES;
    [UIView animateWithDuration:0.2 animations:^{
        _blurView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        _chooseFilterView.alpha = 1;
    }];
}

- (IBAction)hideFeedFilters:(id)sender {
    [UIView animateWithDuration:0.2 animations:^{
        _blurView.frame = CGRectMake(0, 0, self.view.frame.size.width, 40.f);
        _chooseFilterView.alpha = 0;
    } completion:^(BOOL finished) {
        _chooseLocationButtonView.hidden = NO;
        _chooseFilterView.hidden = YES;
    }];
}

- (void)filterFeedByStatus:(NSString*)status{
    if(status){
        _feedTableController.filteredSnacks = [[NSMutableArray alloc] init];
        for(PFObject *snack in _feedTableController.noms){
            //Available status is New and Almost Gone combined
            if([status isEqualToString:@"Available"] &&
               [@[@"New",@"Almost Gone"] containsObject:snack[@"Status"]]){
                [_feedTableController.filteredSnacks addObject:snack];
            }
            //Individiual Statuses
            else if([snack[@"Status"] isEqualToString:status]){
                [_feedTableController.filteredSnacks addObject:snack];
            }
            
            //Reload table when we reach the last item
            if([_feedTableController.noms indexOfObject:snack] == [_feedTableController.noms count]-1){
                NSLog(@"Has snacks: %i", (int)_feedTableController.filteredSnacks.count);
                _emptySetLabel.hidden = [_feedTableController.filteredSnacks count];
                [_availableFeedTableView reloadData];
            }
            else{
                NSLog(@"No snacks");
            }
        }
        [_filterFeedButton setTitle:status forState:UIControlStateNormal];
    }
    else{
        [_filterFeedButton setTitle:@"All" forState:UIControlStateNormal];
        _feedTableController.filteredSnacks = _feedTableController.noms;
        _emptySetLabel.hidden = [_feedTableController.filteredSnacks count];
        [_availableFeedTableView reloadData];
    }
    
    [self hideFeedFilters:self];
}
@end
