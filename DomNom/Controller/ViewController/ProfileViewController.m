//
//  ProfileViewController.m
//  SnackFeed
//
//  Created by Daniel.Burke on 2/8/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import "ProfileViewController.h"
#import "FeedViewController.h"
#import "TransitionProfileToFeed.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated{
    _backButton.layer.cornerRadius = 16.5f;
    self.navigationController.delegate = self;
}

- (void)viewDidDisappear:(BOOL)animated{
    self.navigationController.delegate = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UINavigationController Delegate Methods
- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController
                                  animationControllerForOperation:(UINavigationControllerOperation)operation
                                               fromViewController:(UIViewController *)fromVC
                                                 toViewController:(UIViewController *)toVC {
    NSLog(@"Back to feed");
    if (fromVC == self && [toVC isKindOfClass:[FeedViewController class]]) {
        return [[TransitionProfileToFeed alloc] init];
    }
    else {
        return nil;
    }
}

- (IBAction)dismissProfile:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
