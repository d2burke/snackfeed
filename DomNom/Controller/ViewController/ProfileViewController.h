//
//  ProfileViewController.h
//  SnackFeed
//
//  Created by Daniel.Burke on 2/8/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileViewController : UIViewController
<
UINavigationControllerDelegate
>

@property (weak, nonatomic) IBOutlet UIButton *backButton;

- (IBAction)dismissProfile:(id)sender;

@end
