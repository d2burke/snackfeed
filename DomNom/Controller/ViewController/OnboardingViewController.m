//
//  OnboardingViewController.m
//  SnackFeed
//
//  Created by Daniel.Burke on 1/26/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import "OnboardingViewController.h"
#import "ChooseLocationCell.h"
#import "AppDelegate.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface OnboardingViewController ()

@end

@implementation OnboardingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
    self.view.backgroundColor = UIColorFromRGB(0xefeeea);
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(pushNotificationsCancelled:)
                                                 name:PushNotificationCancelledEvent
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(pushNotificationsEnabled:)
                                                 name:PushNotificationEnabledEvent
                                               object:nil];
    
    _utility = [[Utility alloc] init];
    _locations = [[NSArray alloc] init];
    _signIn = [GPPSignIn sharedInstance];
    _signIn.shouldFetchGooglePlusUser = YES;
    _signIn.shouldFetchGoogleUserEmail = YES;
    _signIn.clientID = @"1048836749363-cb1ljqdsa8pi6u3krugd3meefpn0p198.apps.googleusercontent.com";
    _signIn.scopes = @[ kGTLAuthScopePlusLogin ];
    _signIn.attemptSSO = YES;
    _signIn.delegate = self;
    
    _allowLocationView.layer.cornerRadius = 10.f;
    _chooseLocationView.layer.cornerRadius = 10.f;
    _enableNotificationsView.layer.cornerRadius = 10.f;
    _registerView.layer.cornerRadius = 10.f;
    _doneView.layer.cornerRadius = 10.f;
    
    _allowLocationButton.layer.cornerRadius = 7.f;
    _enableNotificationsButton.layer.cornerRadius = 7.f;
    _registerButton.layer.cornerRadius = 7.f;
    _doneButton.layer.cornerRadius = 7.f;
    
    _changePhotoButton.layer.cornerRadius = 50.f;
    _changePhotoButton.layer.borderColor = GOLD.CGColor;
    _changePhotoButton.layer.borderWidth = 5.f;
    _removePhotoButton.layer.cornerRadius = 15.f;
    
    CALayer *nameUnderline = [CALayer layer];
    CGRect lineFrame = CGRectMake(0, _nameField.frame.size.height-1, _nameField.frame.size.width, 1);
    nameUnderline.frame = lineFrame;
    nameUnderline.backgroundColor = MED_GRAY.CGColor;
    [_nameField.layer addSublayer:nameUnderline];
    
    CALayer *emailUnderline = [CALayer layer];
    lineFrame = CGRectMake(0, _emailField.frame.size.height-1, _emailField.frame.size.width, 1);
    emailUnderline.frame = lineFrame;
    emailUnderline.backgroundColor = MED_GRAY.CGColor;
    [_emailField.layer addSublayer:emailUnderline];
    
    CALayer *passUnderline = [CALayer layer];
    lineFrame = CGRectMake(0, _passwordField.frame.size.height-1, _passwordField.frame.size.width, 1);
    passUnderline.frame = lineFrame;
    passUnderline.backgroundColor = MED_GRAY.CGColor;
    [_passwordField.layer addSublayer:passUnderline];
}

- (void)viewWillAppear:(BOOL)animated{
    _chooseLocationTableController = [[ChooseLocationTableController alloc] init];
    _chooseLocationTableController.tableView = _chooseLocationTableView;
    _chooseLocationTableController.delegate = self;
    _chooseLocationTableController.locations = _locations;
    
    _chooseLocationTableView.delegate = _chooseLocationTableController;
    _chooseLocationTableView.dataSource  = _chooseLocationTableController;
}

- (void)viewDidAppear:(BOOL)animated{
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if([textField isEqual:_nameField]){
        [_emailField becomeFirstResponder];
    }
    else if([textField isEqual:_emailField]){
        [_passwordField becomeFirstResponder];
    }
    else{
        [_passwordField resignFirstResponder];
        [self signUp:_registerButton];
    }
    return NO;
}

#pragma mark - ImagePickerDelegate Methods
- (IBAction)takePhoto:(id)sender{
    _photoWasTaken = YES;
    [self.navigationController setNavigationBarHidden:YES];
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
    [imagePicker setCameraDevice:UIImagePickerControllerCameraDeviceFront];
    [imagePicker setDelegate:self];
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (IBAction)choosePhoto:(id)sender{
    _photoWasTaken = NO;
    [self.navigationController setNavigationBarHidden:YES];
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [imagePicker setDelegate:self];
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (IBAction)removePhoto:(id)sender {
    _addPhotoView.hidden = NO;
    _userImageData = nil;
    [_changePhotoButton setImage:nil forState:UIControlStateNormal];
}

- (IBAction)changePhoto:(id)sender {
    if(_photoWasTaken){
        [self takePhoto:_takePhotoButton];
    }
    else{
        [self choosePhoto:_choosePhotoButton];
    }
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

-(void)imagePickerController:(UIImagePickerController *) picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    __block NSString *imageFileName = nil;
    NSURL *imageURL = [info valueForKey:UIImagePickerControllerReferenceURL];
    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *myasset)
    {
        ALAssetRepresentation *representation = [myasset defaultRepresentation];
        NSString *fileName = [representation filename];
        imageFileName = fileName;
    };
    
    ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
    [assetslibrary assetForURL:imageURL
                   resultBlock:resultblock
                  failureBlock:nil];
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    image = [image fixOrientation];
    
    float oldWidth = image.size.width;
    float scaleFactor = 1280.f / oldWidth;
    
    float newHeight = image.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [image drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    _addPhotoView.hidden = YES;
    
    [self dismissViewControllerAnimated:YES completion:^(void){
        self.navigationController.navigationBarHidden = YES;
        _userImageData = UIImageJPEGRepresentation(newImage, 0.0);
        _changePhotoButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
        [_changePhotoButton setImage:newImage forState:UIControlStateNormal];
    }];
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    if(status != kCLAuthorizationStatusAuthorizedWhenInUse){
        NSLog(@"Throw Error and explain benefits of location");
    }
    [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"DidAllowLocation"];
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [_locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation{
    if(newLocation != _currentLocation){
        _currentLocation = newLocation;
        [_locationManager stopUpdatingLocation];
        
        NSString *endPoint = [NSString stringWithFormat:
                              @"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%f,%f&types=establishment&radius=500&key=AIzaSyBMQgDdx1cDt5-z9q4oygHGRmPweGSq73A", newLocation.coordinate.latitude, newLocation.coordinate.longitude];
        
        [_utility apiCall:endPoint withParameters:nil andMethod:@"GET" completion:^(AFHTTPRequestOperation *operation, id responseObject) {
            _locations = [responseObject objectForKey:@"results"];
            _chooseLocationTableController.locations = _locations;
            [_chooseLocationTableView reloadData];
            
            _onboardingScrollView.contentSize = CGSizeMake(self.view.frame.size.width*2, self.view.frame.size.height);
            _pageControl.hidden = NO;
            _pageControl.numberOfPages = 2;
            [_allowLocationButton setTitle:@"Location Allowed" forState:UIControlStateNormal];
            [_allowLocationButton removeTarget:self action:@selector(requestLocationAccess:) forControlEvents:UIControlEventTouchUpInside];
            
            [UIView animateWithDuration:0.34 animations:^{
                _onboardingScrollView.contentOffset = CGPointMake(self.view.frame.size.width, 0);
            }];
            
        } failure:^(NSError *error) {
            NSLog(@"Google API Error: %@", error);
        }];
    }
    else{
        NSLog(@"Same Location");
    }
}

#pragma mark - Keyboard Methods
-(void)keyboardWillShow:(NSNotification*)notification{
    // keyboard frame is in window coordinates
//    NSDictionary *userInfo = [notification userInfo];
//    CGRect keyboardFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    [UIView animateWithDuration:0.34 animations:^{
    }];
}

-(void)keyboardWillHide:(NSNotification*)notification{
    [UIView animateWithDuration:0.34 animations:^{
    } completion:^(BOOL finished) {
    }];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    CGFloat x = scrollView.contentOffset.x;
    int index = fabs(x/self.view.frame.size.width);
    switch (index) {
        case 0:
            _skipOnboardingButton.hidden = NO;
            break;
            
        default:
            break;
    }
}

#pragma mark - ChooseLocationTableControllerDelegate
- (void)didSelectLocationAtIndex:(NSIndexPath *)indexPath{
    
    //Check if location exists
    NSDictionary *location = [_locations objectAtIndex:indexPath.row];
    _currentLocation = [[CLLocation alloc] initWithLatitude:[[[[location objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lat"] floatValue] longitude:[[[[location objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lat"] floatValue]];
    
    //Check to see if a location exists, if it doesn't create it and return the object
    [PFCloud callFunctionInBackground:@"upsertLocation" withParameters:location block:^(id object, NSError *error) {
        if(!error){
            //Subscribe user to notifications for this location
            _selectedLocation = [object objectForKey:@"Location"];
            [_selectedLocation fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                [[NSUserDefaults standardUserDefaults] setObject:object.objectId forKey:@"SelectedLocationId"];
            }];
            
            NSMutableArray *snacks = [object objectForKey:@"Snacks"];
            NSString *channelName = [NSString stringWithFormat:@"office-%@", _selectedLocation.objectId];
            PFInstallation *currentInstallation = [PFInstallation currentInstallation];
            [currentInstallation addUniqueObject:channelName forKey:@"channels"];
            [currentInstallation saveInBackground];
            
            NSLog(@"Location: %@", object);
            
            if([snacks count]){
                NSLog(@"Found Snacks!: %@", snacks);
            }
        }
        else{
            NSLog(@"Upsert Error: %@", error);
        }
    }];
    
    ChooseLocationCell *cell = (ChooseLocationCell*)[_chooseLocationTableView cellForRowAtIndexPath:indexPath];
    _locationLabel.text = cell.nameLabel.text;
    _onboardingScrollView.contentSize = CGSizeMake(self.view.frame.size.width*3, self.view.frame.size.height);
    _pageControl.numberOfPages = 3;
    
    [UIView animateWithDuration:0.34 animations:^{
        _onboardingScrollView.contentOffset = CGPointMake(self.view.frame.size.width*2, 0);
    }];
}

#pragma mark - GPPSignInDelegate
- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth
                   error: (NSError *) error {
    if(error){
        NSLog(@"Received error %@ and auth object %@",error, auth);
    }
    else{
        GTLServicePlus* plusService = [[GTLServicePlus alloc] init];
        plusService.retryEnabled = YES;
        plusService.authorizer = [_signIn authentication];
        GTLQueryPlus *query = [GTLQueryPlus queryForPeopleGetWithUserId:@"me"];
        
        [plusService executeQuery:query
                completionHandler:^(GTLServiceTicket *ticket,
                                    GTLPlusPerson *person,
                                    NSError *error) {
                    if (error) {
                        GTMLoggerError(@"Error: %@", error);
                    } else {
                        _signOutButton.hidden = NO;
                        NSLog(@"Person: %@ with email: %@", person, _signIn.userEmail);
                        NSLog(@"Organizations: %@", person.organizations);
                        
                        //Automatically register Parse User
                        
                        //Call delegate method to dismiss view
                        
                        //Remove this once delegate is set up
                        [self dismissViewControllerAnimated:YES completion:^{
                            //
                        }];
                    }
                }];
    }
}

- (IBAction)signInWithGoogle:(id)sender {
    [_signIn authenticate];
}

- (IBAction)signOutOfGoogle:(id)sender {
    [_signIn signOut];
    [_signIn disconnect];
}

#pragma mark - Custom

- (IBAction)signUp:(id)sender{
    NSArray *emailComponents = [_emailField.text componentsSeparatedByString:@"@"];
    PFUser *user = [PFUser user];
    user.username = [emailComponents firstObject];
    user.password = _passwordField.text;
    user.email = _emailField.text;
    
    NSArray *nameComponents = [_nameField.text componentsSeparatedByString:@" "];
    user[@"lname"] = [nameComponents objectAtIndex:0];
    user[@"lname"] = ([nameComponents count] > 1) ? [nameComponents objectAtIndex:1] : @"";
    
    if(_userImageData){
        PFFile *imageFile = [PFFile fileWithName:[NSString stringWithFormat:@"%@_profileImage", user.username]
                                            data:_userImageData];
        [imageFile saveInBackground];
        user[@"image"] = imageFile;
    }
    else{
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserImageData"];
    }
    
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            _onboardingScrollView.contentSize = CGSizeMake(self.view.frame.size.width*5, self.view.frame.size.height);
            _pageControl.numberOfPages = 5;
            [_registerButton setTitle:@"Success!" forState:UIControlStateNormal];
            [_registerButton removeTarget:self action:@selector(signUp:) forControlEvents:UIControlEventTouchUpInside];
            
            [UIView animateWithDuration:0.34 animations:^{
                _onboardingScrollView.contentOffset = CGPointMake(self.view.frame.size.width*4, 0);
            }];
        } else {
            NSString *errorString = [error userInfo][@"error"];
            NSLog(@"Error: %@", errorString);
        }
    }];
}

- (IBAction)completeOnboarding:(id)sender {
    NSLog(@"Complete Onboarding");
    if(_selectedLocation){
        [_delegate didCompleteOnboardingAtLocation:_selectedLocation];
    }
    else{
        [_delegate didCompleteOnboarding];
    }
}

- (IBAction)skipOnboarding:(id)sender {
    NSLog(@"Skip Onboarding");
    [_delegate didSkipOnboarding];
}

- (IBAction)requestToEnablePushNotifications:(id)sender {
    // Prompt to Register for Push Notitications
    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                    UIUserNotificationTypeBadge |
                                                    UIUserNotificationTypeSound);
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                             categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

- (void)pushNotificationsCancelled:(NSNotification*)notification{
    [_enableNotificationsButton setTitle:@"Notifications Disabled" forState:UIControlStateNormal];
    _onboardingScrollView.contentSize = CGSizeMake(self.view.frame.size.width*4, self.view.frame.size.height);
    _pageControl.numberOfPages = 4;
    [UIView animateWithDuration:0.34 animations:^{
        _onboardingScrollView.contentOffset = CGPointMake(self.view.frame.size.width*3, 0);
    }];
}

- (void)pushNotificationsEnabled:(NSNotification*)notification{
    [_enableNotificationsButton setTitle:@"Notifications Enabled" forState:UIControlStateNormal];
    _onboardingScrollView.contentSize = CGSizeMake(self.view.frame.size.width*4, self.view.frame.size.height);
    _pageControl.numberOfPages = 4;
    _skipOnboardingButton.hidden = NO;
    [UIView animateWithDuration:0.34 animations:^{
        _onboardingScrollView.contentOffset = CGPointMake(self.view.frame.size.width*3, 0);
    }];
}

- (IBAction)requestLocationAccess:(id)sender {
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    
    if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [_locationManager requestWhenInUseAuthorization];
    }
    else{
        NSLog(@"DOesn't respond");
    }
}

- (void)didDisconnectWithError:(NSError *)error {
    if (error) {
        NSLog(@"Received error %@", error);
    } else {
        NSLog(@"User is disconnected");
        _signOutButton.hidden = YES;
    }
}

@end
