//
//  NomViewController.h
//  SnackFeed
//
//  Created by Daniel.Burke on 1/23/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NomTableController.h"

@interface NomViewController : UIViewController
<
NomTableControllerDelegate,
UIImagePickerControllerDelegate,
UINavigationControllerDelegate
>

@property (strong, nonatomic) PFUser *currentUser;
@property (strong, nonatomic) PFObject *nom;
@property (strong, nonatomic) NSData *imageData;
@property (strong, nonatomic) NSMutableArray *activities;
@property (strong, nonatomic) NomTableController *nomTableController;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentBarBottomContstraint;

@property (weak, nonatomic) IBOutlet UIView *commentBarView;
@property (weak, nonatomic) IBOutlet UIScrollView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *logo;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *userActionLabel;
@property (weak, nonatomic) IBOutlet UILabel *actionTimeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *nomImageView;
@property (weak, nonatomic) IBOutlet UILabel *nomInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *nomDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *nomFloorLabel;
@property (weak, nonatomic) IBOutlet UILabel *nomSectionLabel;
@property (weak, nonatomic) IBOutlet UILabel *nomStatusIcon;
@property (weak, nonatomic) IBOutlet UILabel *nomStatusLabel;
@property (weak, nonatomic) IBOutlet UIButton *sayThankYouButton;
@property (weak, nonatomic) IBOutlet UIButton *sendCommentButton;
@property (weak, nonatomic) IBOutlet UIButton *nomEatenLabel;
@property (weak, nonatomic) IBOutlet UITableView *nomActivityTableView;
@property (weak, nonatomic) IBOutlet UITextField *commentField;
@property (weak, nonatomic) IBOutlet UIButton *takePhotoButton;

- (IBAction)sendComment:(id)sender;
- (IBAction)takePhoto:(id)sender;
@end
