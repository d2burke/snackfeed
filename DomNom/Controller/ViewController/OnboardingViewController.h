//
//  OnboardingViewController.h
//  SnackFeed
//
//  Created by Daniel.Burke on 1/26/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChooseLocationTableController.h"
#import "UIImage+UIImage_fixOrientation_h.h"

@protocol OnboardingViewControllerDelegate <NSObject>

@optional
- (void)didCompleteOnboarding;
- (void)didCompleteOnboardingAtLocation:(PFObject*)location;
- (void)didSkipOnboarding;

@end

@interface OnboardingViewController : UIViewController
<
GPPSignInDelegate,
UITextFieldDelegate,
UIScrollViewDelegate,
CLLocationManagerDelegate,
UIImagePickerControllerDelegate,
UINavigationControllerDelegate,
ChooseLocationTableControllerDelegate
>

@property (strong, nonatomic) Utility *utility;
@property (nonatomic) BOOL photoWasTaken;
@property (strong, nonatomic) id <OnboardingViewControllerDelegate> delegate;
@property (strong, nonatomic) NSData *userImageData;
@property (strong, nonatomic) NSIndexPath *selectedLocationIndexPath;
@property (strong, nonatomic) NSArray *locations;
@property (retain, nonatomic) GPPSignIn *signIn;
@property (strong, nonatomic) PFObject *selectedLocation;
@property (nonatomic, strong) CLLocation *currentLocation;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (strong, nonatomic) ChooseLocationTableController *chooseLocationTableController;

@property (weak, nonatomic) IBOutlet UIView *allowLocationView;
@property (weak, nonatomic) IBOutlet UIView *chooseLocationView;
@property (weak, nonatomic) IBOutlet UIView *enableNotificationsView;
@property (weak, nonatomic) IBOutlet UIView *registerView;
@property (weak, nonatomic) IBOutlet UIView *doneView;
@property (weak, nonatomic) IBOutlet UIView *addPhotoView;
@property (weak, nonatomic) IBOutlet UILabel *addImageInstructionsLabel;
@property (weak, nonatomic) IBOutlet UILabel *registerStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UIButton *signOutButton;
@property (weak, nonatomic) IBOutlet UIButton *allowLocationButton;
@property (weak, nonatomic) IBOutlet UIButton *enableNotificationsButton;
@property (weak, nonatomic) IBOutlet UIButton *changePhotoButton;
@property (weak, nonatomic) IBOutlet UIButton *takePhotoButton;
@property (weak, nonatomic) IBOutlet UIButton *choosePhotoButton;
@property (weak, nonatomic) IBOutlet UIButton *removePhotoButton;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UIButton *skipOnboardingButton;
@property (weak, nonatomic) IBOutlet GPPSignInButton *signInButton;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIScrollView *onboardingScrollView;
@property (weak, nonatomic) IBOutlet UITableView *chooseLocationTableView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

- (IBAction)signInWithGoogle:(id)sender;
- (IBAction)signOutOfGoogle:(id)sender;
- (IBAction)completeOnboarding:(id)sender;
- (IBAction)skipOnboarding:(id)sender;
- (IBAction)requestToEnablePushNotifications:(id)sender;
- (IBAction)requestLocationAccess:(id)sender;
- (IBAction)takePhoto:(id)sender;
- (IBAction)choosePhoto:(id)sender;
- (IBAction)removePhoto:(id)sender;
- (IBAction)changePhoto:(id)sender;
- (IBAction)signUp:(id)sender;
@end
