//
//  CreateViewController.h
//  SnackFeed
//
//  Created by Daniel.Burke on 1/23/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CreateNomTableController.h"
#import "TypeCollectionController.h"

@protocol CreateNomDelegate <NSObject>

@optional
- (void)didCreateNom:(PFObject*)nom;

@end

@interface CreateViewController : UIViewController
<
UIScrollViewDelegate,
UITextFieldDelegate,
CreateNomTableControllerDelegate,
TypeCollectionViewControllerDelegate,
UIImagePickerControllerDelegate,
UINavigationControllerDelegate
>

@property (strong, nonatomic) NSString *nomFloor;
@property (strong, nonatomic) NSString *nomSection;
@property (strong, nonatomic) NSString *nomStatus;
@property (strong, nonatomic) NSString *nomDescription;
@property (strong, nonatomic) NSData *imageData;
@property (strong, nonatomic) NSMutableArray *floors;
@property (strong, nonatomic) NSMutableArray *sections;
@property (strong, nonatomic) PFObject *selectedLocation;
@property (strong, nonatomic) CreateNomTableController *tableController;
@property (strong, nonatomic) TypeCollectionController *typeCollectionController;
@property (strong, nonatomic) id <CreateNomDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *logo;
@property (weak, nonatomic) IBOutlet UIButton *nomImageButton;
@property (weak, nonatomic) IBOutlet UIButton *addOptionButton;
@property (weak, nonatomic) IBOutlet UIButton *statusButton;
@property (weak, nonatomic) IBOutlet UIButton *floorButton;
@property (weak, nonatomic) IBOutlet UIButton *sectionButton;
@property (weak, nonatomic) IBOutlet UIButton *statusNewButton;
@property (weak, nonatomic) IBOutlet UIButton *statusAlmostButton;
@property (weak, nonatomic) IBOutlet UIButton *statusGoneButton;
@property (weak, nonatomic) IBOutlet UITextField *addOptionField;
@property (weak, nonatomic) IBOutlet UITextField *descriptionTextField;
@property (weak, nonatomic) IBOutlet UITableView *createNomTableView;
@property (weak, nonatomic) IBOutlet UIScrollView *createScrollView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *nomDoneButton;
@property (weak, nonatomic) IBOutlet UICollectionView *typeCollectionView;


- (IBAction)takePhoto:(id)sender;
- (IBAction)switchSettingType:(id)sender;
- (IBAction)createNom:(id)sender;

@end
