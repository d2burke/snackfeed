//
//  CreateViewController.m
//  SnackFeed
//
//  Created by Daniel.Burke on 1/23/15.
//  Copyright (c) 2015 Dominion Enterprises. All rights reserved.
//

#import "CreateViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "UIImage+UIImage_fixOrientation_h.h"
#import "FoodTypeCell.h"

@interface CreateViewController (){
    NSArray *types;
}

@end

@implementation CreateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _nomStatus = @"New";
    _nomFloor = @"1";
    _nomSection = @"";
    
    types = @[
              @{@"type":@"Donuts",@"icon":@"∂"},
              @{@"type":@"Pizza",@"icon":@"∞"},
              @{@"type":@"Coffee",@"icon":@"7"},
              @{@"type":@"Sandwiches",@"icon":@"¢"},
              @{@"type":@"Candy",@"icon":@"÷"},
              @{@"type":@"Cookies",@"icon":@"≥"},
              @{@"type":@"Bagals",@"icon":@"≤"},
              @{@"type":@"Cake",@"icon":@"µ"},
              @{@"type":@"Fruit",@"icon":@"æ"},
              @{@"type":@"Veggies",@"icon":@"…"},
              @{@"type":@"Popcorn",@"icon":@"¬"},
              @{@"type":@"Other",@"icon":@"?"}
                ];
    
    _createScrollView.delegate = self;
    _statusButton.layer.cornerRadius = 6.f;
    _floorButton.layer.cornerRadius = 6.f;
    _sectionButton.layer.cornerRadius = 6.f;
    _addOptionButton.layer.cornerRadius = 6.f;
    
    NSDictionary *normalFontDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [UIFont systemFontOfSize:16.f], NSFontAttributeName,
                                    [UIColor grayColor], NSForegroundColorAttributeName, nil];
    NSDictionary *iconFontDict = [NSDictionary dictionaryWithObjectsAndKeys:
                                  [UIFont fontWithName:@"TruckFeed" size:40.f], NSFontAttributeName,
                                  _statusNewButton.titleLabel.textColor, NSForegroundColorAttributeName,
                                  nil];
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc] initWithString:@"å\nNew" attributes:normalFontDict];
    [aAttrString addAttributes:iconFontDict range:NSMakeRange(0, 1)];
    _statusNewButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _statusNewButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [_statusNewButton setAttributedTitle:aAttrString forState:UIControlStateNormal];
    
    iconFontDict = [NSDictionary dictionaryWithObjectsAndKeys:
                    [UIFont fontWithName:@"TruckFeed" size:40.f], NSFontAttributeName,
                    _statusAlmostButton.titleLabel.textColor, NSForegroundColorAttributeName,
                    nil];
    
    aAttrString = [[NSMutableAttributedString alloc] initWithString:@"&\nAlmost Gone" attributes:normalFontDict];
    [aAttrString addAttributes:iconFontDict range:NSMakeRange(0, 1)];
    _statusAlmostButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _statusAlmostButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [_statusAlmostButton setAttributedTitle:aAttrString forState:UIControlStateNormal];
    
    iconFontDict = [NSDictionary dictionaryWithObjectsAndKeys:
                    [UIFont fontWithName:@"TruckFeed" size:40.f], NSFontAttributeName,
                    _statusGoneButton.titleLabel.textColor, NSForegroundColorAttributeName,
                    nil];
    
    aAttrString = [[NSMutableAttributedString alloc] initWithString:@"(\nGone" attributes:normalFontDict];
    [aAttrString addAttributes:iconFontDict range:NSMakeRange(0, 1)];
    _statusGoneButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _statusGoneButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [_statusGoneButton setAttributedTitle:aAttrString forState:UIControlStateNormal];
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"LastFloor"]){
        _nomFloor = [[NSUserDefaults standardUserDefaults] objectForKey:@"LastFloor"];
        [_floorButton setTitle:_nomFloor forState:UIControlStateNormal];
    }
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"LastSection"]){
        _nomSection = [[NSUserDefaults standardUserDefaults] objectForKey:@"LastSection"];
        [_sectionButton setTitle:_nomSection forState:UIControlStateNormal];
    }
}

- (void)viewDidAppear:(BOOL)animated{
    _tableController = [[CreateNomTableController alloc] init];
    _tableController.delegate = self;
    _tableController.floors = _selectedLocation[@"Floors"];
    _tableController.sections = _selectedLocation[@"Sections"];
    _createNomTableView.delegate = _tableController;
    _createNomTableView.dataSource = _tableController;
    [_createNomTableView reloadData];
    
    _typeCollectionController = [[TypeCollectionController alloc] init];
    _typeCollectionController.collectionView = _typeCollectionView;
    _typeCollectionController.delegate = self;
    _typeCollectionView.delegate = _typeCollectionController;
    _typeCollectionView.dataSource = _typeCollectionController;
    _createNomTableView.tableFooterView.hidden = YES;
    
}

- (void)switchSettingType:(id)sender{
    UIButton *button = (UIButton*)sender;
    _tableController.settingIndex = (int)button.tag;
    switch (_tableController.settingIndex) {
        case 0:{
            _statusButton.selected = YES;
            _floorButton.selected = NO;
            _sectionButton.selected = NO;
            _createNomTableView.tableFooterView.hidden = YES;
            break;
        }
        case 1:{
            _statusButton.selected = NO;
            _floorButton.selected = YES;
            _sectionButton.selected = NO;
            _createNomTableView.tableFooterView.hidden = NO;
            _addOptionField.text = @"";
            _addOptionField.placeholder = @"Floor Number";
            break;
        }
        case 2:{
            _statusButton.selected = NO;
            _floorButton.selected = NO;
            _sectionButton.selected = YES;
            _createNomTableView.tableFooterView.hidden = NO;
            _addOptionField.text = @"";
            _addOptionField.placeholder = @"Section Name";
            break;
        }
        default:
            break;
    }
    [_createNomTableView reloadData];
}
- (IBAction)addItem:(id)sender {
    switch ((int)_tableController.settingIndex) {
        case 1:
            if(![_tableController.floors containsObject:_addOptionField.text]){
                [_tableController.floors addObject:_addOptionField.text];
                NSLog(@"Floors: %@", _tableController.floors);
                [_createNomTableView reloadData];
                _selectedLocation[@"Floors"] = _tableController.floors;
                [_selectedLocation saveEventually];
            }
            else{
                NSLog(@"Floor exists");
            }
            break;
        case 2:{
                if(![_tableController.sections containsObject:_addOptionField.text]){
                    [_tableController.sections addObject:_addOptionField.text];
                    [_createNomTableView reloadData];
                    _selectedLocation[@"Sections"] = _tableController.sections;
                    [_selectedLocation saveEventually];
                }
                else{
                    NSLog(@"Section exists on this floor");
                }
            }
            break;
        default:
            NSLog(@"Hmm..?");
            break;
    }
    _addOptionField.text = @"";
    [self setValue:_addOptionField.text forSetting:_tableController.settingIndex];
}

- (IBAction)createNom:(id)sender {
    if(_imageData && [_descriptionTextField.text length]){
        
        PFObject *nom = [PFObject objectWithClassName:@"Nom"];
        nom[@"Office"] = _selectedLocation;
        nom[@"Status"] = (_nomStatus) ? _nomStatus : @"New";
        nom[@"Floor"] = _nomFloor;
        nom[@"Section"] = _nomSection;
        nom[@"Description"] = _descriptionTextField.text;
        
        if([PFUser currentUser]){
            nom[@"User"] = [PFUser currentUser];
        }
        else{
            nom[@"User"] = [PFUser user];
        }
        
        [_statusButton setTitle:@"New" forState:UIControlStateNormal];
        [_floorButton setTitle:@"Floor" forState:UIControlStateNormal];
        [_sectionButton setTitle:@"Section" forState:UIControlStateNormal];
        
        _nomStatus = @"New";
        _nomFloor = @"1";
        _nomSection = @"";
        
        NSLog(@"Snack:%@", nom);
        
        PFFile *imageFile = [PFFile fileWithData:_imageData];
        [imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            [nom addUniqueObject:imageFile forKey:@"images"];
            [nom pinInBackground];
            [nom saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if(!error){
                    NSLog(@"New Snack");
                    [_delegate didCreateNom:nom];
                }
                else{
                    NSLog(@"Save Error");
                }
            }];
        }];
    }
    else{
        NSLog(@"Must complete all fields");
    }
}

#pragma mark - TypeCollectionViewControllerDelegate
- (void)didSelectTypeAtIndexPath:(NSIndexPath *)indexPath{
    _createNomTableView.scrollEnabled = YES;
    _typeCollectionView.hidden = YES;
    if(indexPath.row == 11){
        [_descriptionTextField becomeFirstResponder];
    }
    else{
        _descriptionTextField.text = [[types objectAtIndex:indexPath.row] objectForKey:@"type"];
    }
}

#pragma mark - CreateNomTableControllerDelegate
- (void)didCancelCreateNom{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setValue:(NSString*)value forSetting:(int)settingIndex{
    switch (settingIndex) {
        case 0:{
            _nomStatus = value;
            [_statusButton setTitle:_nomStatus forState:UIControlStateNormal];
            [UIView animateWithDuration:0.34 animations:^{
                [_statusButton sizeToFit];
            }];
            NSLog(@"Status: %@", _nomStatus);
            break;
        }
        case 1:{
            _nomFloor = value;
            [_floorButton setTitle:_nomFloor forState:UIControlStateNormal];
            [UIView animateWithDuration:0.34 animations:^{
                [_floorButton sizeToFit];
            }];
            [[NSUserDefaults standardUserDefaults] setObject:_nomFloor forKey:@"LastFloor"];
            NSLog(@"Floor: %@", _nomFloor);
            break;
        }
        case 2:{
            _nomSection = value;
            [_sectionButton setTitle:_nomSection forState:UIControlStateNormal];
            [UIView animateWithDuration:0.34 animations:^{
                [_sectionButton sizeToFit];
            }];
            [[NSUserDefaults standardUserDefaults] setObject:_nomSection forKey:@"LastSection"];
            NSLog(@"Section: %@", _nomSection);
            break;
        }
        default:
            break;
    }
}

- (void)setValueAtIndex:(int)index forSetting:(int)settingIndex{
    switch (settingIndex) {
        case 0:{
            NSArray *statuses = @[@"New",@"Almost Gone",@"Gone"];
            [self setValue:[statuses objectAtIndex:index] forSetting:settingIndex];
            break;
        }
        case 1:{
            [self setValue:[_selectedLocation[@"Floors"] objectAtIndex:index] forSetting:settingIndex];
            break;
        }
        case 2:{
            [self setValue:[_selectedLocation[@"Sections"] objectAtIndex:index] forSetting:settingIndex];
            break;
        }
        default:
            break;
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return NO;
}


#pragma mark - ImagePickerDelegate Methods
- (IBAction)takePhoto:(id)sender{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
    [imagePicker setDelegate:self];
    [self presentViewController:imagePicker animated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *) picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    __block NSString *imageFileName = nil;
    NSURL *imageURL = [info valueForKey:UIImagePickerControllerReferenceURL];
    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *myasset)
    {
        ALAssetRepresentation *representation = [myasset defaultRepresentation];
        NSString *fileName = [representation filename];
        imageFileName = fileName;
    };
    
    ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
    [assetslibrary assetForURL:imageURL
                   resultBlock:resultblock
                  failureBlock:nil];
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    NSLog(@"Obtained Image");
    
    image = [image fixOrientation];
    
    float oldWidth = image.size.width;
    float scaleFactor = 1280.f / oldWidth;
    
    float newHeight = image.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [image drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    _imageData = UIImageJPEGRepresentation(newImage, 0.0);
    
    _descriptionTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Food Description" attributes:@{NSForegroundColorAttributeName: [[UIColor whiteColor] colorWithAlphaComponent:0.5]}];
    _descriptionTextField.textColor = [UIColor whiteColor];
    
    [self dismissViewControllerAnimated:YES completion:^(void){
        [_nomImageButton setImage:newImage forState:UIControlStateNormal];
        _nomImageButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
        _nomImageButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        _createNomTableView.tableHeaderView.backgroundColor = [UIColor blackColor];
        
        CGRect gradientFrame = _nomImageButton.frame;
        UIColor *colorOne = [UIColor blackColor];
        UIColor *colorTwo = [UIColor clearColor];
        NSArray *colors =  [NSArray arrayWithObjects:(id)colorOne.CGColor, colorTwo.CGColor, nil];
        NSNumber *stopOne = [NSNumber numberWithFloat:0.3];
        NSNumber *stopTwo = [NSNumber numberWithFloat:1];
        NSArray *locations = [NSArray arrayWithObjects:stopOne, stopTwo, nil];
        CAGradientLayer *maskLayer = [CAGradientLayer layer];
        maskLayer.frame = gradientFrame;
        maskLayer.locations = locations;
        maskLayer.colors = colors;
        _nomImageButton.layer.mask = maskLayer;
    }];
}
@end
